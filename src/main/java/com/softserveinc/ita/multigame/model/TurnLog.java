package com.softserveinc.ita.multigame.model;

import java.time.LocalDateTime;

/**
 * @author Igor Khlaponin
 */

public class TurnLog {
    //TODO will be discussed later

    private String player;
    private String turn;
    private LocalDateTime turnTime;

    public TurnLog() {}

    public TurnLog(String player, String turn) {
        this.player = player;
        this.turn = turn;
        this.turnTime = LocalDateTime.now();
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public String getTurn() {
        return turn;
    }

    public void setTurn(String turn) {
        this.turn = turn;
    }

    public LocalDateTime getTurnTime() {
        return turnTime;
    }

    public void setTurnTime(LocalDateTime turnTime) {
        this.turnTime = turnTime;
    }

    @Override
    public String toString() {
        return ""; //TODO make it as JSON
    }
}
