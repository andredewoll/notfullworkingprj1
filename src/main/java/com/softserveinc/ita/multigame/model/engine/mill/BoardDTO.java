package com.softserveinc.ita.multigame.model.engine.mill;

import java.util.List;

class BoardDTO {
    private List<String> board;
    private int countOfWhite;
    private int countOfBlack;
    private String tip;

    BoardDTO(List<String> board, int countOfWhite, int countOfBlack, String tip) {
        this.board = board;
        this.countOfWhite = countOfWhite;
        this.countOfBlack = countOfBlack;
        this.tip = tip;
    }

    public List<String> getBoard() {
        return board;
    }

    public int getCountOfWhite() {
        return countOfWhite;
    }

    public int getCountOfBlack() {
        return countOfBlack;
    }

    public String getTip() {
        return tip;
    }
}
