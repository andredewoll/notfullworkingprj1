package com.softserveinc.ita.multigame.model;

/**
 * @author Igor Khlaponin
 */

public enum Gender {
    MALE,
    FEMALE
}
