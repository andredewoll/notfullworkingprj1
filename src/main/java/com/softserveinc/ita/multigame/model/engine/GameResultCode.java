package com.softserveinc.ita.multigame.model.engine;

public enum GameResultCode {
    OK,
    BAD_PLAYER,
    BAD_FIRST_PLAYER_ORDER,
    BAD_SECOND_PLAYER_ORDER,
    BAD_TURN_ORDER,
    BAD_TURN_LOGIC,
    BAD_TURN_FOR_FINISHED_GAME,
    BAD_TURN_FOR_NOT_STARTED_GAME
}
