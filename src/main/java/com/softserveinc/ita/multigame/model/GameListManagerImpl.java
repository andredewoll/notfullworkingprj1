package com.softserveinc.ita.multigame.model;

import com.softserveinc.ita.multigame.services.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.stream.Collectors.*;

@Component
public class GameListManagerImpl implements GameListManager{

    private Map<Long, Game> gameList = new ConcurrentHashMap<>();

    @Autowired
    PlayerService playerService;

    public GameListManagerImpl(){

        /*Player player1 = new Player(1L, "Vasya","123");
        Player player2 = new Player(2L, "Zhora","123");
        Player player3 = new Player(3L, "Kolya","123");

        Game bat1 = new Game(player1, GameType.SEABATTLE);
        bat1.getGameEngine().setSecondPlayer(player2);
        Game bat2 = new Game(player1, GameType.SEABATTLE);
        Game bat3 = new Game(player2, GameType.SEABATTLE);
        Game bat4 = new Game(player3, GameType.SEABATTLE);
        Game bat5 = new Game(player3, GameType.SEABATTLE);

        gameList.put(bat1.getGameEngine().getId(), bat1);
        gameList.put(bat2.getGameEngine().getId(), bat2);
        gameList.put(bat3.getGameEngine().getId(), bat3);
        gameList.put(bat4.getGameEngine().getId(), bat4);
        gameList.put(bat5.getGameEngine().getId(), bat5);*/
    }

    @Override
    public Game getGame(Long id) {
        return gameList.get(id);
    }

    @Override
    public boolean createGame(Player firstPlayer, GameType gameType) {
        Game game = new Game(playerService.get(firstPlayer.getId()), gameType);
        gameList.put(game.getGameEngine().getId(), game);
        return true;
    }

    @Override
    public Game createAndReturnGame(Player firstPlayer, GameType gameType) {
        Game game = new Game(playerService.get(firstPlayer.getId()), gameType);
        gameList.put(game.getGameEngine().getId(), game);
        return game;
    }

    @Override
    public boolean deleteGame(Long id) {
        Game game = gameList.remove(id);
        return game != null;
    }

    @Override
    public List<Long> getCreatedGamesIds(Player player) {
        return gameList.entrySet().stream()
                .filter(entry -> entry.getValue().getGameEngine().getFirstPlayer().equals(player)
                                    && entry.getValue().getGameEngine().getSecondPlayer() == null)
                .map(Map.Entry::getKey)
                .collect(toCollection(ArrayList::new));
    }

    @Override
    public List<Long> getPlayingGamesIds(Player player) {
        return gameList.entrySet().stream()
                .filter(entry -> entry.getValue().getGameEngine().getFirstPlayer().equals(player)
                                    && entry.getValue().getGameEngine().getSecondPlayer() != null
                                    || player.equals( entry.getValue().getGameEngine().getSecondPlayer() ) )
                .map(Map.Entry::getKey)
                .collect(toCollection(ArrayList::new));
    }

    @Override
    public List<Long> getWaitingGamesIds(Player player) {
        return gameList.entrySet().stream()
                .filter(entry -> entry.getValue().getGameEngine().getSecondPlayer() == null
                        && !player.equals(entry.getValue().getGameEngine().getFirstPlayer()))
                .map(Map.Entry::getKey)
                .collect(toCollection(ArrayList::new));
    }

    @Override
    public List<Long> getAllIds() {
        return gameList.keySet().stream().collect(toCollection(LinkedList::new));
    }
}
