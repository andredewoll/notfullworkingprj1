package com.softserveinc.ita.multigame.model.engine.halma;

/**
 * Created by vvserdiuk on 06.10.2016.
 */
public enum CellState {
    BUSY_BY_FIRST_PLAYER,
    BUSY_BY_SECOND_PLAYER,
    EMPTY
}

