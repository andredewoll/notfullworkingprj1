package com.softserveinc.ita.multigame.model.engine.mill;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameState;
import com.softserveinc.ita.multigame.model.engine.GenericGameEngine;

public class MillGameEngine extends GenericGameEngine {
    private Board board = new Board();
    private MillPlayer millPlayer1 = new MillPlayer("white");
    private MillPlayer millPlayer2 = new MillPlayer("black");

    @Override
    public BoardDTO getBoard() {
        return new BoardDTO(board.drawBoard(),
                millPlayer1.getCountOfStonesInHand(),
                millPlayer2.getCountOfStonesInHand(),
                getGameState().toString());
    }

    @Override
    protected boolean validateTurnLogic(String turn) {
        MillPlayer player;
        if (gameState == GameState.WAIT_FOR_FIRST_PLAYER_TURN
                || gameState == GameState.WAIT_FOR_FIRST_PLAYER_DROP){
            player = millPlayer1;
        } else {
            player = millPlayer2;
        }

        switch (gameState){
            case WAIT_FOR_FIRST_PLAYER_TURN:
            case WAIT_FOR_SECOND_PLAYER_TURN:
                if (turnParser(turn).length == 1){
                    return putLogicValidation(player, turn);
                } else {
                    return replaceLogicValidation(player, turn);
                }
            case WAIT_FOR_FIRST_PLAYER_DROP:
            case WAIT_FOR_SECOND_PLAYER_DROP:
                return dropLogicValidation(player, turn);
            default:
                return false;
        }
    }

    @Override
    protected GameState changeGameState(Player player, String turn) {
        doTurn(player, turn);

        if (board.isNewMillCreate()) {
            if (player.equals(getFirstPlayer())) {
                return GameState.WAIT_FOR_FIRST_PLAYER_DROP;
            } else {
                return GameState.WAIT_FOR_SECOND_PLAYER_DROP;
            }
        }

        if (millPlayer1.getCountOfStones() < 3) {
            return GameState.FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER;
        }
        if (millPlayer2.getCountOfStones() < 3) {
            return GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER;
        }

        if (player.equals(getFirstPlayer())) {
            return GameState.WAIT_FOR_SECOND_PLAYER_TURN;
        } else {
            return GameState.WAIT_FOR_FIRST_PLAYER_TURN;
        }
    }

    void doTurn(Player player, String turn) {
        MillPlayer currentPlayer;
        MillPlayer opponent;

        if (player.equals(getFirstPlayer())) {
            currentPlayer = millPlayer1;
            opponent = millPlayer2;
        } else {
            currentPlayer = millPlayer2;
            opponent = millPlayer1;
        }

        int[] positions = turnParser(turn);

        switch (gameState){
            case WAIT_FOR_FIRST_PLAYER_TURN:
            case WAIT_FOR_SECOND_PLAYER_TURN:
                if (turnParser(turn).length == 1){
                    putStone(currentPlayer.getNewStone(), positions[0]);
                } else {
                    replaceStone(positions[0], positions[1]);
                }
                break;
            case WAIT_FOR_FIRST_PLAYER_DROP:
            case WAIT_FOR_SECOND_PLAYER_DROP:
                opponent.dropStone(dropStone(positions[0]));
                break;
        }
    }

    private void putStone(Stone stone, int position) {
        Position p = board.getPosition(position);
        p.setStone(stone);
    }

    private void replaceStone(int oldPosition, int newPosition) {
        Position oldP = board.getPosition(oldPosition);
        Stone stone = oldP.getStone();
        oldP.setStone(null);
        board.isMillDestroy();
        Position newP = board.getPosition(newPosition);
        newP.setStone(stone);
    }

    private Stone dropStone(int position) {
        Position p = board.getPosition(position);
        Stone stone = p.getStone();
        p.setStone(null);
        board.isMillDestroy();
        return stone;
    }

    private int[] turnParser(String turn) {
        String[] split = turn.split(",");
        int[] position = new int[split.length];
        for (int i = 0; i < split.length; i++) {
            position[i] = Integer.parseInt(split[i]);
        }
        return position;
    }

    private boolean putLogicValidation(MillPlayer player, String turn) {
        int[] positions = turnParser(turn);
        Position p = board.getPosition(positions[0]);
        return (gameState == GameState.WAIT_FOR_FIRST_PLAYER_TURN
                || gameState == GameState.WAIT_FOR_SECOND_PLAYER_TURN)
                && player.getCountOfStonesInHand() > 0
                && p.getStone() == null;
    }

    private boolean replaceLogicValidation(MillPlayer player, String turn) {
        int[] positions = turnParser(turn);
        Position oldP = board.getPosition(positions[0]);
        Position newP = board.getPosition(positions[1]);
        return (gameState == GameState.WAIT_FOR_FIRST_PLAYER_TURN
                || gameState == GameState.WAIT_FOR_SECOND_PLAYER_TURN)
                && player.getCountOfStonesInHand() == 0
                && oldP.getStone() != null
                && oldP.getStone().getColor().equals(player.getColor())
                && newP.getStone() == null
                && (oldP.getNeighbors().contains(newP) || player.getCountOfStones() == 3);
    }

    private boolean dropLogicValidation(MillPlayer player, String turn) {
        int[] positions = turnParser(turn);
        Position p = board.getPosition(positions[0]);

        boolean isAllPositionInMill = true;
        for (Position position : board.getPositions()) {
            if (position.getStone() != null && !position.getStone().getColor().equals(player.getColor())) {
                isAllPositionInMill = isAllPositionInMill && board.isPositionInMill(position);
            }
        }
        return (gameState == GameState.WAIT_FOR_FIRST_PLAYER_DROP
                || gameState == GameState.WAIT_FOR_SECOND_PLAYER_DROP)
                && p.getStone() != null
                && !p.getStone().getColor().equals(player.getColor())
                && (!board.isPositionInMill(p) || isAllPositionInMill);
    }



    //TODO only for test;
    void setGameState(GameState state){
        gameState = state;
    }
    void setBoard(Board board) {
        this.board = board;
    }
    void setMillPlayer1(MillPlayer millPlayer1) {
        this.millPlayer1 = millPlayer1;
    }

}
