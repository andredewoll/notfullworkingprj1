package com.softserveinc.ita.multigame.model.engine.halma;

/**
 * Created by vvserdiuk on 03.10.2016.
 */
public class Cell {
        CellState state = CellState.EMPTY;
        String address;

        public Cell(String address) {
                this.address = address;
        }

        public CellState getState() {
                return state;
        }

        public void setState(CellState state) {
                this.state = state;
        }

        public String getAddress() {
                return address;
        }

        public void setAddress(String address) {
                this.address = address;
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;

                Cell cell = (Cell) o;

                return address.equals(cell.address);

        }

        @Override
        public int hashCode() {
                return address.hashCode();
        }

        @Override
        public String toString() {
                return "" + address + "," + state + " ";
        }
}
