package com.softserveinc.ita.multigame.controllers.seabattle;

import com.softserveinc.ita.multigame.controllers.GameListController;
import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameType;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.seabattle.Cell;
import com.softserveinc.ita.multigame.services.GameListService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/seabattle")
public class SeaBattlePageController {
    private Logger logger = Logger.getLogger(GameListController.class);
    @Autowired
    private GameListService gameListService;

    @RequestMapping(method = RequestMethod.GET)
    public String getPage(){
        return "seaBattle/gameInfo";
    }

    @ResponseBody
    @RequestMapping(value = "/show", method = RequestMethod.GET)
    public List<Cell> showBoard1(@SessionAttribute("player") Player player, @RequestParam("id") Long gameId) throws IOException {
        Game game = gameListService.getGame(gameId);
        List<Cell> board;
        if(game==null){//3
            board = new ArrayList<>();//3
            Cell cell = new Cell();//3
            cell.setAddress("TheEnd");//3
            cell.setValue("TheEnd");//3
            board.add(cell);//3
            return board;//3
        }//3
        else {//3
            board = (List<Cell>) game.getGameEngine().getBoard(player);
            return board;
        }//3
    }

    @ResponseBody
    @RequestMapping(value = "/show2", method = RequestMethod.GET)
    public List<Cell> showBoard2(@SessionAttribute("player") Player player, @RequestParam("id") Long gameId) throws IOException {
        Game game = gameListService.getGame(gameId);
        Player anotherPlayer;
        if(player.equals(game.getGameEngine().getFirstPlayer()))
            anotherPlayer = game.getGameEngine().getSecondPlayer();
        else
            anotherPlayer = game.getGameEngine().getFirstPlayer();
        List<Cell> board = (List<Cell>) gameListService.getGame(gameId).getGameEngine().getBoard(anotherPlayer);
        return board;
    }

    @ResponseBody
    @RequestMapping(value = "/makeTurn", method = RequestMethod.GET)
    public List<Cell> makeTurn(@SessionAttribute Player player,
                         @RequestParam("gameId") Long gameId,
                         @RequestParam("turn") String turn){
        logger.info("MakeTurn gameId is: " + gameId + " turn is: " + turn);
        gameListService.makeTurn(gameId, player, turn);
        Game game = gameListService.getGame(gameId);

        List<Cell> board;
        if(game==null){//3
            board = new ArrayList<>();//3
            Cell cell = new Cell();//3
            cell.setAddress("TheEnd");//3
            cell.setValue("TheEnd");//3
            board.add(cell);//3
            return board;//3
        }//3
        else {//3
            Player anotherPlayer;
            if (player.equals(game.getGameEngine().getFirstPlayer()))
                anotherPlayer = game.getGameEngine().getSecondPlayer();
            else
                anotherPlayer = game.getGameEngine().getFirstPlayer();
            board = (List<Cell>) gameListService.getGame(gameId).getGameEngine().getBoard(anotherPlayer);
            return board;
        }//3
    }

    @RequestMapping("/final")
    public String getFinalPage(){
        return "seaBattle/tmpFinalPage";
    }
}
