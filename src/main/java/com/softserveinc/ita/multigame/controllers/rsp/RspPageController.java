package com.softserveinc.ita.multigame.controllers.rsp;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameEngine;
import com.softserveinc.ita.multigame.model.engine.GameState;
import com.softserveinc.ita.multigame.services.GameListService;
import com.softserveinc.ita.multigame.services.utils.JSONParcer;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping("/rsp")
public class RspPageController {
    private Logger logger = Logger.getLogger(RspPageController.class);

    @Autowired
    GameListService gameListService;

    @RequestMapping(method = RequestMethod.GET)
    public String getPage(@SessionAttribute Player player, @RequestParam("gameId") Long gameId, Model model) {

        GameEngine gameEngine = gameListService.getGame(gameId).getGameEngine();
        Player firstPlayer = gameEngine.getFirstPlayer();
        Player secondPlayer = gameEngine.getSecondPlayer();
        gameEngine.setFirstPlayer(firstPlayer);
        gameEngine.setSecondPlayer(secondPlayer);
        Player winner = gameEngine.getTheWinner();

        if (player.equals(gameEngine.getFirstPlayer())){
            secondPlayer = gameEngine.getSecondPlayer();
        } else {
            secondPlayer = gameEngine.getFirstPlayer();
        }

        model.addAttribute("firstPlayer", firstPlayer == null ? "" : firstPlayer);
        model.addAttribute("secondPlayer", secondPlayer == null ? "" : secondPlayer);
        model.addAttribute("gameId", gameId);
        model.addAttribute("currentPlayer", player);
        logger.debug("Current player is  " + player + "  " + "1st player is "
                + firstPlayer + " 2nd player is  " + secondPlayer);
        if (winner != null) {
            logger.debug("Winner is   " + winner);
        }
        return "rsp/rspGame";
    }

   /* @ResponseBody
    @RequestMapping(value = "/refreshTurn", method = RequestMethod.GET)
    public String refreshPlayerTurn(@RequestParam("gameId") Long gameId) {
        GameEngine gameEngine = gameListService.getGame(gameId).getGameEngine();
        GameState gameState = gameEngine.getGameState();
        Player firstPlayer = gameEngine.getFirstPlayer();
        Player secondPlayer = gameEngine.getSecondPlayer();

        String newGameState = "";
        switch (gameState) {
            case WAIT_FOR_SECOND_PLAYER:
                newGameState = "wait for 2nd player";
                break;
            case WAIT_FOR_FIRST_PLAYER_TURN:
                newGameState = "wait while " + firstPlayer.getLogin() + " makes turn";
                break;
            case WAIT_FOR_SECOND_PLAYER_TURN:
                newGameState = "wait while " + secondPlayer.getLogin() + " makes turn";
                break;
            case FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER:
                newGameState = "The winner is " + firstPlayer.getLogin() + ". Congratulations!";
                break;
            case FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER:
                newGameState = "The winner is " + secondPlayer.getLogin() + ". Congratulations!";
                break;
            case FINISHED_WITH_DRAW:
                newGameState = " It is a draw, sorry:) !";
                break;
            default:
                break;
        }
        return JSONParcer.toJson(newGameState);
    }*/
   @ResponseBody
   @RequestMapping(value = "/refreshTurn", method = RequestMethod.GET)
   public String refreshPlayerTurn(@SessionAttribute Player player, @RequestParam("gameId") Long gameId) {
       logger.debug("refreshPlayerTurn");
       return JSONParcer.toJson(gameListService.getTips(gameId, player));
   }

}
