package com.softserveinc.ita.multigame.controllers.halma;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameEngine;
import com.softserveinc.ita.multigame.model.engine.GameResultCode;
import com.softserveinc.ita.multigame.model.engine.halma.Cell;
import com.softserveinc.ita.multigame.services.GameListService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServlet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/halma")
public class HalmaPageController extends HttpServlet{

    private Logger logger = Logger.getLogger(HalmaPageController.class);

    @Autowired
    private GameListService gameListService;
    private Game game;
    private GameEngine gameEngine;
    private List<Cell> board;

    @RequestMapping(method = RequestMethod.GET)
    public String getPage(@RequestParam("gameId") Long gameId,
                          @RequestParam(value = "result", required = false) String result,
                          Model model, @SessionAttribute Player player){
        game = gameListService.getGame(gameId);

        String rival = null;
        String color = null;

        if (game.getGameEngine().getFirstPlayer().equals(player)){
            rival = game.getGameEngine().getSecondPlayer().getLogin();
            color = "green";
        }
        else if (game.getGameEngine().getSecondPlayer().equals(player)){
            rival = game.getGameEngine().getFirstPlayer().getLogin();
            color = "blue";
        }

        model.addAttribute("color", color);
        model.addAttribute("result", result);
        model.addAttribute("gameId", gameId);
        model.addAttribute("currentPlayer", player);
        model.addAttribute("rival", rival);

        return "halma/halmaPage";
    }

    @ResponseBody
    @RequestMapping(value = "/ajax/board", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Object> getBoard(@RequestParam("gameId") Long gameId)  {
        logger.info("In getBoard gameId is: " + gameId);
        game = gameListService.getGame(gameId);
        gameEngine = game.getGameEngine();
        GameResultCode resultCode = gameEngine.getResultCode();
        board = (List<Cell>) gameEngine.getBoard();

        Map<String, Object> data = new HashMap<>();
        data.put("resultCode", resultCode);
        data.put("board", board);

        return data;
    }
}
