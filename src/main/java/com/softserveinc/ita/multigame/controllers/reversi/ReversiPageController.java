package com.softserveinc.ita.multigame.controllers.reversi;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameState;
import com.softserveinc.ita.multigame.services.GameHistoryService;
import com.softserveinc.ita.multigame.services.GameListService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * @author Igor Khlaponin
 */

@Controller
@RequestMapping("/reversi")
public class ReversiPageController {

    private Logger logger = Logger.getLogger(ReversiPageController.class);

    @Autowired
    GameListService gameListService;
    @Autowired
    GameHistoryService gameHistoryService;

    @RequestMapping(method = RequestMethod.GET)
    public String getCurrentReversiGamePage(@SessionAttribute Player player,
                                            @RequestParam("gameId") Long gameId, Model model) {

        Game currentGame = gameListService.getGame(gameId);

        if (currentGame.getGameEngine().getFirstPlayer().equals(player) &&
                currentGame.getGameEngine().getSecondPlayer() == null) {
            logger.info(player.getLogin() + " is waiting for second player");
            model.addAttribute("opponent", "nobody");
        } else if (!currentGame.getGameEngine().getFirstPlayer().equals(player) &&
                currentGame.getGameEngine().getSecondPlayer() == null) {
            logger.info(player.getLogin() + " connected to the created game #" + gameId);
            gameListService.getGame(gameId).getGameEngine().setSecondPlayer(player);

            String opponent = (gameListService.getGame(gameId).getGameEngine().getFirstPlayer().equals(player))
                    ? gameListService.getGame(gameId).getGameEngine().getSecondPlayer().getLogin()
                    : gameListService.getGame(gameId).getGameEngine().getFirstPlayer().getLogin();
            model.addAttribute("opponent", opponent);
            model.addAttribute("currentPlayer", player.getLogin());
        }

        model.addAttribute("currentPlayer", player.getLogin());
        model.addAttribute("matrix", gameListService.getGame(gameId).getGameEngine().getBoard());
        model.addAttribute("gameId", gameId);
        model.addAttribute("resultCode", gameListService.getGame(gameId).getGameEngine().getResultCode());
        String color = (gameListService.getGame(gameId)).getGameEngine().getFirstPlayer().equals(player)
                ? "white"
                : "black";
        model.addAttribute("pawnColor", color);


        return "reversi/reversiGamePage";
    }

    @ResponseBody
    @RequestMapping(value = "/refreshField", method = RequestMethod.GET)
    public String refreshField(@RequestParam("gameId") Long gameId) {
        return (String) gameListService.getGame(gameId).getGameEngine().getBoard();
    }


    @ResponseBody
    @RequestMapping(value = "/getCurrentStatus", method = RequestMethod.GET)
    public String getCurrentStatus(@RequestParam("gameId") Long gameId) {
        String gameStateToShow = "";
        Game currentGame = gameListService.getGame(gameId);
        Player firstPlayer = currentGame.getGameEngine().getFirstPlayer();
        Player secondPlayer = currentGame.getGameEngine().getSecondPlayer();

        GameState gameState = currentGame.getGameEngine().getGameState();
        if (gameState == GameState.WAIT_FOR_FIRST_PLAYER_TURN) {
            gameStateToShow = "Please, wait while " + firstPlayer.getLogin() + " makes turn... ";
        } else if (gameState == GameState.WAIT_FOR_SECOND_PLAYER_TURN) {
            gameStateToShow = "Please, wait while " + secondPlayer.getLogin() + " makes turn... ";
        } else if (gameState == GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER) {
            gameStateToShow = firstPlayer.getLogin() + "is a winner!";
        } else if (gameState == GameState.FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER) {
            gameStateToShow = secondPlayer.getLogin() + "is a winner!";
        } else if (gameState == GameState.FINISHED_WITH_DRAW) {
            gameStateToShow = "Draw!";
        }

        return gameStateToShow;
    }


    @ResponseBody
    @RequestMapping(value = "/getResultCode", method = RequestMethod.GET)
    public String getGameResultCode(@RequestParam("gameId") Long gameId) {
        return gameListService.getGame(gameId).getGameEngine().getResultCode().toString();
    }

}
