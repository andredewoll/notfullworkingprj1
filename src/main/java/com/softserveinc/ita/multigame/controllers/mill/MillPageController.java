package com.softserveinc.ita.multigame.controllers.mill;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameEngine;
import com.softserveinc.ita.multigame.services.GameListService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/mill")
public class MillPageController {
    private Logger logger = Logger.getLogger(MillPageController.class);

    @Autowired
    private GameListService gameListService;

    @RequestMapping(method = RequestMethod.GET)
    public String getPage(@SessionAttribute Player player,
                          @RequestParam("gameId") Long gameId,
                          Model model){

        Game game = gameListService.getGame(gameId);
        GameEngine gameEngine = game.getGameEngine();

        if (player.equals(gameEngine.getFirstPlayer()) && gameEngine.getSecondPlayer() == null) {
            logger.debug(player.getLogin() + " connect to created game");
            model.addAttribute("opponent", "");
        }
        else if ((player.equals(gameEngine.getFirstPlayer()) && gameEngine.getSecondPlayer() != null)
                || player.equals(gameEngine.getSecondPlayer())) {
            logger.debug(player.getLogin() + " connect to created game");

            Player opponent;
            if (player.equals(gameEngine.getFirstPlayer())){
                opponent = gameEngine.getSecondPlayer();
            } else {
                opponent = gameEngine.getFirstPlayer();
            }
            model.addAttribute("opponent", opponent);
        }

        model.addAttribute("gameId", gameId);
        return "mill/millGame";
    }

    @ResponseBody
    @RequestMapping(value = "/refreshBoard", method = RequestMethod.GET)
    public Object refreshBoard(@RequestParam("gameId") Long gameId) {
        Game game = gameListService.getGame(gameId);
        return game.getGameEngine().getBoard();
    }


}
