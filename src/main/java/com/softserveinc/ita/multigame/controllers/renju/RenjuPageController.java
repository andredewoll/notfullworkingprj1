package com.softserveinc.ita.multigame.controllers.renju;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameEngine;
import com.softserveinc.ita.multigame.services.GameListService;
import com.softserveinc.ita.multigame.services.utils.JSONParcer;

@Controller
@RequestMapping("/renju")
public class RenjuPageController {
	private Logger logger = Logger.getLogger(RenjuPageController.class);

	@Autowired
	private GameListService gameListService;

	@RequestMapping(method = RequestMethod.GET)
	public String getPage(@SessionAttribute Player player, @RequestParam("gameId") Long gameId, Model model) {
		logger.debug("getPage");
		Game game = gameListService.getGame(gameId);
		GameEngine gameEngine = game.getGameEngine();

		Player firstPlayer = gameEngine.getFirstPlayer();
		Player secondPlayer = gameEngine.getSecondPlayer();

		model.addAttribute("firstPlayer", firstPlayer == null ? "" : firstPlayer.getLogin());
		model.addAttribute("secondPlayer", secondPlayer == null ? "" : secondPlayer.getLogin());

		model.addAttribute("gameId", gameId);
		model.addAttribute("currentPlayer", player);
		logger.debug("getPage1");
		return "renju/renjuGame";
	}
	
	@ResponseBody
    @RequestMapping(value = "/refreshBoard", method = RequestMethod.GET)
    public String refreshBoard(@RequestParam("gameId") Long gameId) {
		logger.debug("refreshBoard");
		Game game = gameListService.getGame(gameId);
		if (game == null){
			return "GAME OVER";
		}
        return (String) game.getGameEngine().getBoard();
    }
	
	@ResponseBody
    @RequestMapping(value = "/refreshPlayerTurn", method = RequestMethod.GET)
    public String refreshPlayerTurn(@SessionAttribute Player player, @RequestParam("gameId") Long gameId) {
		logger.debug("refreshPlayerTurn");		
		return JSONParcer.toJson(gameListService.getTips(gameId, player));
    }

}
