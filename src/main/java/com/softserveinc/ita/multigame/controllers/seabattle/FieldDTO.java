package com.softserveinc.ita.multigame.controllers.seabattle;

import com.softserveinc.ita.multigame.model.engine.seabattle.Cell;

import java.util.List;

public class FieldDTO {
    private Long id;
    private List<Cell> field;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Cell> getField() {
        return field;
    }

    public void setField(List<Cell> field) {
        this.field = field;
    }
}
