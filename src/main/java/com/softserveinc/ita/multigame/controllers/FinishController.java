package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.GameHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;

@Controller
public class FinishController {

    @Autowired
    GameHistoryService gameHistoryService;

    @RequestMapping("/finish")
    public String finish(@SessionAttribute("player")Player player, Model model,
                         @RequestParam("gameId") Long gameId){
        Player winner = gameHistoryService.getWinner(gameId);
        Player loser  = gameHistoryService.getLoser(gameId);

        if (player.equals(winner)){
            model.addAttribute("message", "Congratulations, you won!");
            model.addAttribute("image", "resources/img/win.png");
        }
        else if (player.equals(loser)){
            model.addAttribute("message", "Sorry, you lose. Try your best next time!");
            model.addAttribute("image", "resources/img/lose.png");
        }
        else {
            model.addAttribute("message", "Wow, draw! You're awesome but your opponent is also.");
            model.addAttribute("image", "resources/img/draw.png");
        }
        return "finish";
    }
}
