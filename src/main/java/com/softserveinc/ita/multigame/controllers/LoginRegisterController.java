package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameType;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.GameListService;
import com.softserveinc.ita.multigame.services.PlayerService;
import com.softserveinc.ita.multigame.services.utils.PasswordEncryptor;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.LocalDateTime;

@Controller
@SessionAttributes("player")
public class LoginRegisterController {

    private Logger logger = Logger.getLogger(LoginRegisterController.class);

    @Autowired
    PlayerService playerService;

    @RequestMapping("/")
    public String startPage(){
        return "index";
    }

    @RequestMapping("/registration")
    public String registerPage(){
        return "registration";
    }

    @ModelAttribute("player")
    public Player createPlayerModel() {
        return new Player();
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerNewPlayer(@ModelAttribute("player") @Valid Player player,
                                    BindingResult bindingResult,
                                    ModelMap map){

        if (bindingResult.hasErrors()) {
            logger.info("Errors in during field fulfilling. Returning to registration form");
            return "registration";
        }
        String password = player.getPassword();
        player.setPassword(PasswordEncryptor.getEncryptedPassword(password));
        player.setRegistrationTime(LocalDateTime.now());
        playerService.saveOrUpdate(player); //updates current user if it exists TODO fix it
        logger.info("Success registration form fulfilling. Returning to login page");
        map.addAttribute("player", player);
        return "redirect:/";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String loginSuccess(@RequestParam String login,
                               @RequestParam String password,
                               ModelMap map){
        /** checking security activities*/

        Player player = playerService.getByLogin(login);
        if (player == null) {
            return "redirect:/registration";
        }
        if (PasswordEncryptor.checkPassword(password, player)) {
            map.addAttribute("player", player);
            return "redirect:/list";
        } else {
            return "redirect:/";
        }
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpSession session) {
        if (session != null) {
            session.invalidate();
        }
        logger.info("current player removed from the session. Redirect to index page");
        return "redirect:/";
    }
}
