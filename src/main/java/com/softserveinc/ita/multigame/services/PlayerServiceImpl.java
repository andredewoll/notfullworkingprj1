package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.repositories.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class PlayerServiceImpl implements PlayerService{

    @Autowired
    PlayerRepository playerRepository;
    @Autowired
    GameHistoryService gameHistoryService;

    @Transactional(readOnly = true)
    @Override
    public Player get(Long id) {
        return playerRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    @Override
    public Player getByLogin(String login) {
        return playerRepository.findByLogin(login);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Player> getAll() {
        return playerRepository.findAll();
    }

    //TODO make integration tests when add Admin
    @Override
    public void delete(Player player) {
        gameHistoryService.removeGameHistoriesForPlayer(player);
        playerRepository.delete(player);
    }

    //TODO make integration tests when add Admin
    @Override
    public void delete(Long id) {
        Player player = playerRepository.findOne(id);
        gameHistoryService.removeGameHistoriesForPlayer(player);
        playerRepository.delete(id);
    }

    @Override
    public Player saveOrUpdate(Player player) {
        return playerRepository.save(player);
    }
}
