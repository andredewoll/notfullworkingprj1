package com.softserveinc.ita.multigame.services.utils;

import com.google.gson.Gson;

/**
 * @author Igor Khlaponin
 */

public class JSONParcer {

    public static String toJson(Object object) {
        Gson gson = new Gson();
        return gson.toJson(object);
    }

    public static Object fromJson(String json, Class clazz) {
        Gson gson = new Gson();
        return gson.fromJson(json, clazz);
    }
}
