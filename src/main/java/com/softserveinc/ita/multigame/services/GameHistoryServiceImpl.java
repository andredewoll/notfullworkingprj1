package com.softserveinc.ita.multigame.services;


import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameState;
import com.softserveinc.ita.multigame.repositories.GameHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class GameHistoryServiceImpl implements GameHistoryService {

    @Autowired
    private GameHistoryRepository gameHistoryRepository;
    GameState gameState;

    @Override
    public void saveGameHistoryForGame(Game game) {
        GameHistory gameHistory = new GameHistory(game);
        gameHistoryRepository.save(gameHistory);
    }

    @Override
    @Transactional(readOnly = true)
    public List<GameHistory> getGameHistoriesForPlayer(Player player) {
        return gameHistoryRepository.findByPlayer(player);
    }

    @Override
    @Transactional(readOnly = true)
    public GameHistory getGameHistoryById(Long gameId) {
        return gameHistoryRepository.findOne(gameId);
    }

    @Override
    public void removeGameHistoriesForPlayer(Player player) {
        List<GameHistory> gameHistories = gameHistoryRepository.findByPlayer(player);
        for (GameHistory gh: gameHistories) {
            if (gh.getFirstPlayer().equals(player)){
                gh.setFirstPlayer(null);
            }
            if(gh.getSecondPlayer().equals(player)){
                gh.setSecondPlayer(null);
            }
        }
        gameHistoryRepository.save(gameHistories);
    }

    @Override
    public Player getWinner(Long gameId) {
        GameState gameState = gameHistoryRepository.findOne(gameId).getGameState();
        if (gameState == GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER){
            return gameHistoryRepository.findOne(gameId).getFirstPlayer();
        }
        else if(gameState == GameState.FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER){
            return gameHistoryRepository.findOne(gameId).getSecondPlayer();
        }
        else return null;
    }

    @Override
    public Player getLoser(Long gameId) {
        GameState gameState = gameHistoryRepository.findOne(gameId).getGameState();
        if (gameState == GameState.FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER){
            return gameHistoryRepository.findOne(gameId).getFirstPlayer();
        }
        else if (gameState == GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER){
            return gameHistoryRepository.findOne(gameId).getSecondPlayer();
        }
        else return null;
    }
}
