package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.model.Player;

import java.util.List;

public interface PlayerService {

    Player get(Long id);
    Player getByLogin(String login);
    List<Player> getAll();
    void delete(Player player);
    void delete(Long id);
    Player saveOrUpdate(Player player);
}
