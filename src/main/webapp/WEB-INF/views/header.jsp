<nav class="navbar navbar-default" id="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#navbar" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="list">
                <img alt="multigame" src="${pageContext.request.contextPath}/resources/img/logo/multigame.png"
                     height="25px">
            </a>
        </div>

        <div class="collapse navbar-collapse" id="navbar">
            <ul class="nav navbar-nav">
                <li id="gameList" class=""><a href="list"><span class="glyphicon glyphicon-list">&nbsp;</span>Game List</a>
                </li>
                <li id="profile"><a href="profile?id=${player.id}"><span class="glyphicon glyphicon-user">&nbsp;</span>Profile</a></li>
                <li id="info" class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false">Information <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a data-toggle="modal" data-target="#rules">Rules</a></li>
                        <li><a href="players">Players</a></li>
                        <li class="divider"></li>
                        <li><a data-toggle="modal" data-target="#about">About authors</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <div class="btn-locale">
                        <button type="button" class="btn btn-default btn-xs btn-block active">en</button>
                        <button type="button" class="btn btn-default btn-xs btn-block">ru</button>
                    </div>
                </li>
                <li><a href="logout"><span class="glyphicon glyphicon-log-out">&nbsp;</span>Log Out</a></li>
            </ul>
        </div>
    </div>
</nav>

<!-- About - modal window -->
<div class="modal fade" id="about">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">About</h4>
            </div>
            <div class="modal-body">
                <h5>The project was made by:</h5>
                <p>Igor Khlaponin - Reversi</p>
                <p>Danil Sokyrynskyi - Sea Battle</p>
                <p>Artem Dvornichenko - Renju</p>
                <p>Misha Shvets - Mill</p>
                <p>Vladimir Serdiuk - Halma</p>
                <p>Andrey Volyk - PSP</p>
            </div>
        </div>
    </div>
</div>

<!-- Rules - modal window -->
<div class="modal fade" id="rules">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Rules</h4>
            </div>
            <div class="modal-body">

                <div class="media">
                    <div class="media-left">
                        <img class="media-object rule-img"
                             src="${pageContext.request.contextPath}/resources/img/logo/main/reversi.png"
                             alt="reversi">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Reversi</h4>
                        <p>
                            Some text
                        </p>
                        <p>
                            Some text
                        </p>
                    </div>
                </div>

                <div class="media">
                    <div class="media-left">
                        <img class="media-object rule-img"
                             src="${pageContext.request.contextPath}/resources/img/logo/main/mill.png"
                             alt="mill">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Mill</h4>
                        <p>
                            The Mill Game is a strategy board game for two players also known as Nine Man's Morris.
                            This is a game in which either player can be a winner or loser. Are you ready for
                            it?
                        </p>
                        <p>
                            Brief rules: The board consists of a grid with twenty-four position. Each player
                            has nine stones, colored black or white. Players make turn one after another trying
                            to form 'mills' - three of their own stones lined horizontally or vertically.
                            'Mills' allowing a player to remove an opponent's stone from the game.
                            A player wins when his opponent has only to two stones.
                        </p>
                    </div>
                </div>

                <div class="media">
                    <div class="media-left">
                        <img class="media-object rule-img"
                             src="${pageContext.request.contextPath}/resources/img/logo/main/renju.png"
                             alt="renju">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Renju</h4>
                        <p>
                            Some text
                        </p>
                        <p>
                            Some text
                        </p>
                    </div>
                </div>

                <div class="media">
                    <div class="media-left">
                        <img class="media-object rule-img"
                             src="${pageContext.request.contextPath}/resources/img/logo/main/halma.png"
                             alt="halma">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Halma</h4>
                        <p>
                            Some text
                        </p>
                        <p>
                            Some text
                        </p>
                    </div>
                </div>

                <div class="media">
                    <div class="media-left">
                        <img class="media-object rule-img"
                             src="${pageContext.request.contextPath}/resources/img/logo/main/rsp.png"
                             alt="rsp">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Rock Scissors Paper–</h4>
                        <p>
                            Some text
                        </p>
                        <p>
                            Some text
                        </p>
                    </div>
                </div>

                <div class="media">
                    <div class="media-left">
                        <img class="media-object  rule-img"
                             src="${pageContext.request.contextPath}/resources/img/logo/main/seabattle.png"
                             alt="seabattle">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Sea Battle</h4>
                        <p>
                            Some text
                        </p>
                        <p>
                            Some text
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<%--
</div>--%>
