<%@ page import="com.softserveinc.ita.multigame.model.Game" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Game List</title>
    <link rel="stylesheet" href="https://bootswatch.com/paper/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.1.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/gameLists.js"></script>
</head>
<body>

<%@include file="header.jsp" %>

<div class="container  brn-holder">
    <h2>Hello, ${player.getLogin()}!</h2>

    <a class="btn btn-primary btn-reversi col-xs-6 col-sm-4 col-md-2" href="create?gameType=REVERSI">
        <img class="btn-img" src="${pageContext.request.contextPath}/resources/img/logo/logo_reversi.png">REVERSI</a>

    <a class="btn btn-primary btn-mill col-xs-6 col-sm-4 col-md-2" href="create?gameType=MILL">
        <img class="btn-img" src="${pageContext.request.contextPath}/resources/img/logo/logo_mill.png">MILL</a>

    <a class="btn btn-primary btn-renju col-xs-6 col-sm-4 col-md-2" href="create?gameType=RENJU">
        <img class="btn-img" src="${pageContext.request.contextPath}/resources/img/logo/logo_renju.png">RENJU</a>

    <a class="btn btn-primary btn-halma col-xs-6 col-sm-4 col-md-2" href="create?gameType=HALMA">
        <img class="btn-img" src="${pageContext.request.contextPath}/resources/img/logo/logo_halma.png">HALMA</a>

    <a class="btn btn-primary btn-rsp col-xs-6 col-sm-4 col-md-2" href="create?gameType=RSP">
        <img class="btn-img" src="${pageContext.request.contextPath}/resources/img/logo/logo_rsp.png">RSP</a>

    <a class="btn btn-primary btn-seabattle col-xs-6 col-sm-4 col-md-2" data-toggle="modal" data-target="#myModal">
        <img class="btn-img" src="${pageContext.request.contextPath}/resources/img/logo/logo_seabattle.png">SEA
        BATTLE</a>

    <!--Modal Window for SeaBattle-->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add new game</h4>
                </div>
                <div class="modal-body">
                    You're going to create a new game as a first player. Do you want default emplacement of ships or
                    custom one?
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <a href="create?gameType=SEABATTLE">
                            <button type="button" class="btn btn-primary">Default</button>
                        </a>
                        <a href="createPersonal">
                            <button type="button" class="btn btn-default">Personal</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div><!--end of Modal Window for SeaBattle-->
</div>

<div class="container">
    <h3>Game list</h3>
    <div id="playingGames" class="col-md-4">
    </div>
    <div id="createdGames" class="col-md-4">
    </div>
    <div id="waitingGames" class="col-md-4">
    </div>
</div>
<br>

</body>
</html>