<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Game profile</title>
		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/renju/renju.css" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
				
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/renju/refreshBoard.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/renju/doubleClickOnBoard.js"></script>
	</head>
	
	<body>
		<div class="container myrow-container">
			<div class="panel panel-success">
				<div class="panel-heading">
				<h3>Current player: <a href="#">${currentPlayer.login}</a></h3>
				<h2>Game #${gameId}</h2>
				</div>
				
				<div class="panel-body">
					<div class="col-sm-6">
						<div>
							<h2>First player: <a href="#">${firstPlayer}</a></h2>
							<h2>Second player: <a href="#">${secondPlayer}</a></h2>
						</div>
						
						<div id="playerTurn">
						</div>
						
						<input id="gameId" type="hidden" name="gameId" value="${gameId}" />

						<form action="list" method="get">
						    <input type="submit" id="ToGameList" class="btn btn-default" value="Back to Game List"/>
						</form>

					</div>
		
					<div class="col-sm-6" id="board">						
					</div>
					
				</div>	
			</div>			    		
		</div>		
	</body>

</html>