<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Current Game</title>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/reversi/reversiFieldStyle.css"/>
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/reversi/field.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/reversi/currentpageonload.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/reversi/getCurrentGameStatus.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/reversi/reversiOnClickMakeTurn.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/reversi/getResultCode.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/reversi/refreshField.js"></script>
  <style>
    #player1 {
      color: lawngreen;
    }
    #player2 {
      color: orangered;
    }
    .status {
      font-size: medium;
    }
  </style>
</head>
<body>


<div><h2 style="color: #81b2ff">Reversi game #${gameId}</h2></div>
<div><h3><span id="player1">${currentPlayer}</span> vs. <span id="player2">${opponent}</span></h3></div>

<div class="status"><span id="status"></span> ${currentPlayer}, you are playing with <b>${pawnColor}</b></div>

<div id="turnField"><%--Turn--%>
  <form id="turnForm" action="maketurn" method="post">
    <input id="turn" type="hidden" name="turn" />
  </form>
</div>

<div>
  <div style="float: left">Result code: </div>
  <span id="resultCode"></span>
</div>


<div id="field"> <%--Field--%>
</div>

<input type="hidden" id="idValue" value='${gameId}'/>
<input type="hidden" id="matrix" value='"${matrix}"'/>


<div>
  <form method="get" action="list">
    <input type="submit" class="btn btn-info" value="Back to the games list">
  </form>
</div>

<div id="fieldbackground">
  <img src="${pageContext.request.contextPath}/resources/img/reversi/chessboard_background.jpg"
       alt="board background"/>
</div>

</body>
</html>
