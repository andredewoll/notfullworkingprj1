<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="springForm" uri="http://www.springframework.org/tags/form" %>
<html>
<head>

    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css"/>
    <link rel="stylesheet" href="https://bootswatch.com/paper/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <title>${displayedPlayer.login}</title>
</head>
<body>
<div class="container">

    <div style="float: left">
        <img src="${pageContext.request.contextPath}/resources/img/profile_logo.png" alt="profile pic"/>
    </div>

    <div>
        <h2>Player info</h2>
    </div>
    <div id="info" style="float: left">
        <div class="header"><b>Login: </b><span id="login">${displayedPlayer.login}</span></div>
        <div class="header"><b>Full Name: </b><span id="fullName">${displayedPlayer.fullName}</span></div>
        <div class="header"><b>Email: </b><span id="email">${displayedPlayer.email}</span></div>
        <div class="header"><b>Gender: </b><span id="gender">${displayedPlayer.gender}</span></div>
        <div class="header"><b>Birthday: </b><span id="birthday">${displayedPlayer.birthdayDate}</span>
        </div>
        <div class="header"><b>Registration time: </b><span
                id="registration">${displayedPlayer.registrationTime}</span></div>
    </div>

    <div>
        <c:if test='${player.login eq displayedPlayer.login}'>
            <span id="history">Games History</span>
            <c:forEach items="${gameHistoryList}" var="game">
                <li><a href="#">
                    <c:out value="Game #${game.id}"/>
                </a></li>
            </c:forEach>
        </c:if>
    </div>


    <%--http://v4-alpha.getbootstrap.com/components/modal/--%>
    <div style="clear: left">
        <button data-target="#updateProfile" role="button" class="btn btn-success" data-toggle="modal">Update</button>
        <a href="list"><button role="button" class="btn btn-default">Back to game list</button></a>
    </div>
</div>



<%--Update modal window--%>
<div class="modal fade" id="updateProfile">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">${player.login}, please, update your profile data</h4>
            </div>
            <div class="modal-body">

                <form id="profileInfoForm" class="form-inline" action="updateProfile" method="post">
                    <input type="hidden" name="login" value="${player.login}"/>

                    <div class="placeholder">Full Name:<input name="fullName" class="form-control"
                                                              placeholder="Please enter your full name"/></div>

                    <div class="placeholder">Email:<input type="email" name="email" class="form-control"
                                                          placeholder="Please enter your email"/></div>

                    <div class="placeholder">Gender:
                        <select name="gender">
                            <option value="null">Choose your gender</option>
                            <option value="MALE">Male</option>
                            <option value="FEMALE">Female</option>
                        </select>
                    </div>

                    <div class="placeholder">Birthday:<input type="date" name="birthday" class="form-control"/>
                    </div>

                    <div style="clear: left"><input class="btn btn-success" type="submit" value="Update"/></div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Back to your profile</button>
            </div>
        </div>
    </div>
</div>


</body>
</html>