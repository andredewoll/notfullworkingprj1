<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Game Info</title>

  <link href="${pageContext.request.contextPath}/resources/css/seaBattle/gameinfo.css" rel="stylesheet">
  <link href="${pageContext.request.contextPath}/resources/css/seaBattle/bootstrap.min.css" rel="stylesheet">
  <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/seaBattle/target.ico" width="16" height="16" type="image/x-icon">

  <script src="${pageContext.request.contextPath}/resources/js/seaBattle/jquery-3.1.1.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/seaBattle/gameInfo.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/seaBattle/bootstrap.min.js"></script>
</head>

<body>
<div class="container-fluid bs-cont">
  <!--Top Block-->
  <div class="row bs-row">
    <div class="col-md-4 bs-col-top">
      <ul class="nav nav-pills">
        <li><a href="#"><span class="glyphicon glyphicon-list">&nbsp;</span>Game List</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-home">&nbsp;</span>Profile</a></li>
        <li class="active"><a href="#">GAME</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true">Information<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Rules</a></li>
            <li><a href="#">Players</a></li>
            <li><a href="#">Partners</a></li>
            <li class="divider"></li>
            <li><a href="#">About author</a></li>
          </ul>
        </li>
      </ul>
    </div>
    <div class="col-md-4 bs-col-top">
      <h1>Game # ${param.gameId} !</h1>
      <h2>Vasya vs. Zhora</h2>
      <h3>(You're ${player})</h3>
    </div>
    <div class="col-md-4 bs-col-top"></div>
  </div><!--End Of Top Block-->

  <input type="hidden" id="idOfGame" value="${param.gameId}"/>

  <!--Middle Block-->
  <div class="row bs-row-main">
    <div class="col-md-6 bs-col-c">
      <h4>Your field</h4>
      <!-- Field -->
      <div class="jumbotron jumbo-c" id="board1"></div><!-- End of Field -->
    </div>

    <div class="col-md-6 bs-col-c">
      <h4>Enemy field</h4>
      <!-- Score Field -->
      <div class="jumbotron jumbo-c" id="board2"></div><!-- End of Score Field -->
    </div>

  </div><!--End Of Middle Block-->

  <br/>

  <!--Bot Block-->
  <div class="row bs-row">
    <!--Refresh Button-->
    <div class="col-md-4 bs-col">
    </div><!--End of Refresh Button-->

    <!--Warnings-->
    <div class="col-md-4 bs-col" id="botBar">
    </div> <!--End of Warnings-->

    <!--Button & Modal-->
    <div class="col-md-4 bs-col">
      <!--Button-->
      <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal3"><span class="glyphicon glyphicon-chevron-left">&nbsp;</span>Back to list</button>
      <!--Modal-->
      <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Are you sure?</h4>
            </div>
            <div class="modal-body">
              Click OK if you want to close this page and go back to games' list
            </div>
            <div class="modal-footer">
              <a href="#"><button type="submit" class="btn btn-primary">OK</button></a>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
          </div>
        </div>
      </div>
    </div> <!--end Button & Modal-->
  </div><!--End Of Bot Block-->

</div>

</body>
</html>
