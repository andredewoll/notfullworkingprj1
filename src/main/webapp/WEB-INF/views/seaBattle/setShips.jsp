<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Set Ships</title>
    <link href="${pageContext.request.contextPath}/resources/css/seaBattle/set.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/seaBattle/bootstrap.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/seaBattle/target.ico" width="16" height="16" type="image/x-icon">

    <script src="${pageContext.request.contextPath}/resources/js/seaBattle/jquery-3.1.1.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/seaBattle/jquery-ui.js"></script>
   <%-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js"></script>--%>
    <script src="${pageContext.request.contextPath}/resources/js/seaBattle/setShips.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/seaBattle/bootstrap.min.js"></script>
</head>
<body>

<div class="container-fluid bs-cont">
    <!--Top-->
    <div class="row bs-row">
        <div class="col-md-4 bs-col-top">
            <ul class="nav nav-pills">
                <li class="disabled"><a href="#"><span class="glyphicon glyphicon-list">&nbsp;</span>Game List</a></li>
                <li class="disabled"><a href="#"><span class="glyphicon glyphicon-home">&nbsp;</span>Profile</a></li>
                <li class="active"><a href="#">GAME</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true">Information<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Rules</a></li>
                        <li><a href="#">Players</a></li>
                        <li><a href="#">Partners</a></li>
                        <li class="divider"></li>
                        <li><a href="#">About author</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="col-md-4 bs-col-top"><h1>You're daredevil, ${player} !</h1>
            <h3>Let's set your ships! (game id = ${gameId})</h3>
        </div>
        <div class="col-md-4 bs-col-top"></div>
    </div><!--End of Top-->
    <!--Middle-->
    <div class="row bs-row-c">
        <div class="col-md-2 bs-col"></div>

        <div class="col-md-6 bs-col">
            <div class="jumbotron jumbo-c" id="board">
                <!--Field Here-->
            </div>
        </div>

        <div class="col-md-3 bs-col">
            <div class="jumbotron jumbo-c-r">
                <input type="hidden" id="idOfGame" value="${gameId}"/>

                <div id="sideBar">

                </div>

                <br/>
                <a href="#"><button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-remove">&nbsp;</span>Cancel</button></a>
                <br/><br/><br/><br/><br/><br/><br/><br/>
                <button type="submit" class="btn btn-info" onclick="confirm()"><span class="glyphicon glyphicon-saved">&nbsp;</span>Confirm</button>
                <br/>
                <a href="default?gameId=${gameId}"><button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-wrench">&nbsp;</span>Default</button></a>

            </div>
        </div>
        <div class="col-md-1 bs-col">
        </div>
    </div><!--End of Middle-->

</div>
</body>
</html>