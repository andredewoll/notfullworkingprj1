<%@ taglib prefix="springForm" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>Registration page</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" />
    <link rel="stylesheet" href="https://bootswatch.com/paper/bootstrap.min.css">
    <style>
        .error {
            color: #ff0000;
            font-size: medium;
        }
        .placeholder {
            clear:left;
            float:left;
        }
        .message {
            float: left;
            vertical-align: text-bottom;
        }
    </style>
</head>
<body>
<div class="container">
    <springForm:form class="form-inline" commandName="player" name="register" method="post" action="register">
    	<h3>Please register first and enjoy!</h3>

            <div class="placeholder"><springForm:input path="login" cssClass="form-control" placeholder="Please enter your login"/></div>
            <div class="message"><springForm:errors path="login" cssClass="error" /></div>

            <div class="placeholder"><springForm:input path="password" cssClass="form-control" placeholder="Please enter your password"/></div>
            <div class="message"><springForm:errors path="password" cssClass="error" /></div>

            <div class="placeholder"><springForm:input path="email" cssClass="form-control" placeholder="Please enter your email"/></div>
            <div class="message"><springForm:errors path="email" cssClass="error" /></div>

            <div style="clear: left"><input class="btn btn-success" type="submit" value="Register" /></div>

    </springForm:form>
</div>
</body>
</html>
