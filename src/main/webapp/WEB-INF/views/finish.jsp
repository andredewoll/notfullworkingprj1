<%@ page import="com.softserveinc.ita.multigame.model.Game" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Finish Page</title>
    <link rel="stylesheet" href="https://bootswatch.com/paper/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.1.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>
<%@include file="header.jsp" %>
<div class="container  brn-holder">
    <h2>${message}</h2>
    <h3>Click <a href="${pageContext.request.contextPath}/list">here </a> to play another game.</h3>
    <div class="container jumbotron">
    <img src="${image}" style="width: 100%">
    </div>
</div>

</body>
</html>
