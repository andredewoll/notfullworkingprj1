<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Welcome page</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" />
    <link rel="stylesheet" href="https://bootswatch.com/paper/bootstrap.min.css">
</head>
<body>

<div class="container">
    <form class="form-inline" name="login" method="post" action="login">

        <h3>Welcome to Multigame!</h3>
        <input class="form-control" type="text" name="login" placeholder="Please enter your login">
        <input class="form-control" type="password" name="password" placeholder="Please enter your password">
        <input class="btn btn-primary" type="submit" value="Login" />

        <a class="btn btn-success" href="registration">Register</a>
    </form>
</div>
</body>
</html>
