<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List, com.softserveinc.ita.multigame.model.engine.halma.Cell" %>
<%@ page import="com.softserveinc.ita.multigame.model.engine.halma.CellState" %>
<html>
<head>
    <link rel="stylesheet" href="https://bootswatch.com/paper/bootstrap.min.css">
    <script   src="http://code.jquery.com/jquery-3.1.1.min.js"
              integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
              crossorigin="anonymous"></script>
    <script   src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
              integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
              crossorigin="anonymous"></script>
    <script src="${pageContext.request.contextPath}/resources/js/halma/gameBoard.js"></script>

    <title>HalmaGame</title>
</head>
<body>
<nav class="navbar navbar-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">HalmaGame</a>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
            <ul class="nav navbar-nav">
                <li><a href="/halmagame/player?id=${sessionScope.get("playerId")}">Profile</a></li>
                <li><a href="/halmagame/gameslist">Games</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/halmagame/logout">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>
<a  style="margin-right:5%" href="#" class="navbar-left">
    <img src="${pageContext.request.contextPath}/resources/img/halma/logo.png"></a>
<div class="container">
    <h3>Game #${gameId}</h3>
    <h3>Your rival is ${rival}. Your color is ${color}</h3>
    <div>
       <span id="result"></span>
    </div>

    <form id="turnForm" class="form-inline" name="turnForm" method="post" action="">
        <input id="gameId" type="hidden" name="gameId" value="${gameId}">

        <input id="makeTurnField" class="form-control" type="text" name="turn" placeholder="Make turn. Syntax: 'A1,A2'">
        <input id="makeTurnButton" class="btn btn-success" type="submit" value="Make turn"/>
    </form>

    <div id="board">
    </div>
</div>
</body>
</html>
