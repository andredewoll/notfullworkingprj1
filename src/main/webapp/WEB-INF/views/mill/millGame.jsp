<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Mill Game</title>
    <link rel="stylesheet" href="https://bootswatch.com/paper/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mill/mill.css"/>
    <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.1.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/jquery-ui.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/mill/ajax.js"></script>
</head>
<body>

<%@include file="../header.jsp" %>

<div id="welcome">
    <div id="logo">
        <img id="img-logo" alt="mill" src="${pageContext.request.contextPath}/resources/img/logo/main/mill.png/">
    </div>

    <div id="welcome-text">
        <h2>Mill Game #${gameId}. You vs <a href="profile?id=${opponent.getId()}">${opponent.getLogin()}</a></h2>
        <h3> Have a good luck!</h3>
    </div>
</div>

<hr>

<input id="gameId" type="hidden" name="gameId" value="${gameId}">

<div id="field">
    <div id="tips"></div>
    <div id="white"></div>
    <div id="board">
        <div class="position" id="0"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position" id="1"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position" id="2"></div>
        <div class="position"></div>
        <div class="position" id="8"></div>
        <div class="position"></div>
        <div class="position" id="9"></div>
        <div class="position"></div>
        <div class="position" id="10"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position" id="16"></div>
        <div class="position" id="17"></div>
        <div class="position" id="18"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position" id="7"></div>
        <div class="position" id="15"></div>
        <div class="position" id="23"></div>
        <div class="position"></div>
        <div class="position" id="19"></div>
        <div class="position" id="11"></div>
        <div class="position" id="3"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position" id="22"></div>
        <div class="position" id="21"></div>
        <div class="position" id="20"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position" id="14"></div>
        <div class="position"></div>
        <div class="position" id="13"></div>
        <div class="position"></div>
        <div class="position" id="12"></div>
        <div class="position"></div>
        <div class="position" id="6"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position" id="5"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position" id="4"></div>
    </div>
    <div id="black"></div>
</div>
</body>
</html>
