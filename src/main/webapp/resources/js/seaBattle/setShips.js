$( init );
function init() {
    printEmptyBoard();
}
var Field;
function printEmptyBoard(){
    $.ajax({
        method: 'GET',
        url: 'printEmpty',
        success: function (data1) {
            Field = data1;
            $('#board').empty();
            $('#board').append('<h2><img class="target" src="resources/img/seaBattle/target.png" width="40" height="40"/>'+
                '<span class="digit">1</span>' +
                '<span class="digit">2</span> ' +
                '<span class="digit">3</span>' +
                '<span class="digit">4</span> ' +
                '<span class="digit">5</span> ' +
                '<span class="digit">6</span> ' +
                '<span class="digit">7</span> ' +
                '<span class="digit">8</span> ' +
                '<span class="digit">9</span> ' +
                '<span class="digit">10</span></h2>');
            var letters = [ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
            $.each(data1, function(index, element){
                if(index%10==0){
                    var l = letters[index/10];
                    $('#board').append('<span class="letter" style="font-size: 26px;">'+ l +'</span>');
                }
               $('#board').append('<img id=\"'+element.address +'\" class="celly" src="resources/img/seaBattle/anchor2.png" width="40" height="40"/>');
               makeBoardDaggableAndDroppable();
                for(var x=0; x<10; x++) {
                    if (index == (9 + 10 * x)) {
                        $('#board').append('<br/>');
                    }
                }
            });
        },
        error: function () {
            alert('Error while printing EmptyBoard!');
        }
    });
    printSideBar();
}
function confirm(){
    var info = {
        id: $('#idOfGame').val(),
        field: Field
    };
    $.ajax({
        method: 'POST',
        url: 'confirm',
        contentType: 'application/json',
        data: JSON.stringify(info),
        success: function (data2) {
            if(data2 == "field is ok"){
                document.location.href='list';  /** TODO: MB wrong URL??*/
            }
            else{
                alert("field is not ok");
            }
        },
        error: function () {
            alert("Error while getting field from controller!!!");
        }
    });
}
function printSideBar(){
    $('#sideBar').empty();
    for(var x=0; x<20; x++) {
        $('#sideBar').append('<img class="cell" src="resources/img/seaBattle/ship3.png" width="40" height="40"/>');
    }

}
function makeBoardDaggableAndDroppable() {
    $('.cell').draggable({
        containment: "window",
        revert: 'invalid'
    });

    $('.celly').droppable({
        drop: handleDropEvent
    });
}
function handleDropEvent( event, ui ) {
    var draggable = ui.draggable;
    var droppedOn = $(this);
   // alert( 'The place ID "' + droppedOn.attr('id'));
    $.each(Field, function(index, element){
       if(element.address == droppedOn.attr('id')){
           element.value = "#";
       }
    });

    ui.draggable.position( { of: $(this), my: 'left top', at: 'left top' } );
    ui.draggable.draggable( 'disable' );
    $(this).droppable( 'disable' );
}