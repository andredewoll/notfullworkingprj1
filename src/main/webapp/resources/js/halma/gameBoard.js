$(document).ready(function() {
    refreshBoard();
    makeTurn();
    setInterval(function() {
        refreshBoard()
    }, 5000); //5 seconds
});



function refreshBoard() {
    $.ajax({
        method: 'GET',
        url: 'halma/ajax/board?gameId=' + $('#gameId').val(),
        success: function (data) {
            drawBoard(data);
        }
    });
}

//TODO turn by button; remove before demo
function makeTurn(){

    $('#turnForm').submit(function(e) {
        $.ajax({
            type: "POST",
            url: 'halma/ajax/board',
            data: $("#turnForm").serialize(),
            success: function(data) {
                console.log("success");
            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
}

function drawBoard(data) {
    $('#result').html(data.resultCode);

    $('#board').html('<h4 style="word-spacing:18px;"> A B C D E F G H I J K L M N O P</h4>');

    $.each(data.board, function (idx, cell) {
        if (idx != 0 && idx % 16 == 0) {
            $('#board').append('<span style="font-size:15pt">' + idx / 16 + '</span>');
            $('#board').append("<br>");
        }

        if (cell.state == "EMPTY") {
            $('#board').append('<img id="' + cell.address+ '" class="cell" src=resources/img/halma/emptycell.png>');
        }
        if (cell.state == "BUSY_BY_FIRST_PLAYER") {
            $('#board').append('<img id="' + cell.address+ '" class="cell" src=resources/img/halma/greencell.png>')
        }
        if (cell.state == "BUSY_BY_SECOND_PLAYER") {
            $('#board').append('<img id="' + cell.address+ '" class="cell" src=resources/img/halma/bluecell.png>')
        }
    });

    $('#board').append('<span style="font-size:15pt">16</span>');

    makeBoardDaggableAndDroppable();
}

function makeBoardDaggableAndDroppable() {
    $('.cell').draggable({
        containment: "parent",
        revert: 'invalid'
    });

    $('.cell').droppable({
        drop: function (ev, ui) {
            var dragged = ui.draggable;
            var droppedOn = $(this);

            var draggedColor = dragged.attr('src');
            var droppedOnColor = droppedOn.attr('src');
            var draggedId = dragged.attr('id');
            var droppedOnId = droppedOn.attr('id');

            $(droppedOn).droppable("disable");
            $(dragged).droppable("enable");
            $(dragged).attr('src', droppedOnColor);
            $(droppedOn).attr('src', draggedColor);


            var turn = draggedId + ',' + droppedOnId;
            var gameId = $('#gameId').attr('value');

            makeTurnViaAjax(gameId, turn);
        }
    });

    $('.cell').dblclick(function () {
        console.log('in dblClick');
        var gameId = $('#gameId').attr('value');
        var turn = $(this).attr('id') + ',' + $(this).attr('id');
        console.log(turn);
        makeTurnViaAjax(gameId, turn);
    });
}

function makeTurnViaAjax(gameId, turn) {
    $.ajax({
        type: "POST",
        url: 'ajax/maketurn',
        data: {gameId: gameId, turn: turn},
        success: function (data) {
            console.log("success");
            refreshBoard();
        },
        error: function () {
            alert('Error!');
        }
    });
}

