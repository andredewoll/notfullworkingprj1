$(document).ready(function() {
        $('.image').click(function () {
            var turn = $(this).attr('id');
            var gameId = $('#gameId').attr('value');
        console.log(gameId);
        console.log(turn);
        makeTurnClick(gameId, turn);
        refreshPlayerTurn();
    });
    setInterval(function() {
        refreshPlayerTurn();
    }, 3000); //3 seconds
});

function makeTurnClick(g, t) {
    $.ajax({
        type: "POST",
        url: 'ajax/maketurn',
        data: {gameId: g, turn: t},
        success: function (data) {
            console.log("success")
        },
        error: function () {
            console.log('Error with ajax/makeTurn' );
            console.log(g);
            console.log(t);
        }
    });
}

function refreshPlayerTurn() {
    $
        .ajax({
            method: 'GET',
            url: 'rsp/refreshTurn',
            data: 'gameId=' + $('#gameId').attr('value'),
            success: function (data) {
                var tip = $.parseJSON(data).message;
                $('#playerTurn').html(
                    '<h2 style="color:red">'
                    + tip
                    + '</h2>');
            },
            error: function (errorThrown) {
                console.log("Error with /rsp/refreshTurn: " + errorThrown);
            }
        });
}
