'use strict';

function refreshField() {

    $.ajax({
        type: "GET",
        url: '/multigame/reversi/refreshField',
        data: 'gameId=' + $('#idValue').val(),
        dataType: 'text',
        success: function (responseText) {
            $('#matrix').val('\"' + responseText + '\"');
            clearField();
            drawField();
            syncField(repackField());
            refreshGameStatus();
            refreshResultCode();
        },
        error: function (responseText) {
            console.log("bad connection to server");
        }
    });

}
