'use strict';

$(document).ready(function(){
    drawField();
    syncField(repackField());
    refreshGameStatus();
    setInterval(function() {
        refreshField()
    }, 3000);
});
