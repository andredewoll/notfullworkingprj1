function refreshGameStatus() {
    $.ajax({
        method: 'GET',
        url: '/multigame/reversi/getCurrentStatus',
        data: 'gameId=' + $('#idValue').val(),
        success: function (data) {
            $('#status').text(data);
        },
        error: function (errorThrown) {
            console.log("Error: " + errorThrown);
        }
    });
}
