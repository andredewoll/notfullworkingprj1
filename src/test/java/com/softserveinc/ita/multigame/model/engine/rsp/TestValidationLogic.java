package com.softserveinc.ita.multigame.model.engine.rsp;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TestValidationLogic {

    @Test
    public void MockTestingValidationTurnLogic() {
        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper();

        assertTrue(rockScissorsPaper.validateTurnLogic("SPOCK"));
    }
}
