package com.softserveinc.ita.multigame.model.engine.rsp;


import com.softserveinc.ita.multigame.model.Player;
import org.junit.Before;
import org.junit.Test;

import static com.softserveinc.ita.multigame.model.engine.rsp.RockScissorsPaper.happensDraw;
import static com.softserveinc.ita.multigame.model.engine.rsp.RockScissorsPaper.secondPlayerWins;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestSecondPlayerWins {
    Player player1;
    Player player2;
    RockScissorsPaper rockScissorsPaper;

    @Before
    public void setUp() {

        player1 = new Player(1L, "Vasya", "1111");
        player2 = new Player(2L, "Petya", "1111");
        rockScissorsPaper = new RockScissorsPaper(1L);
    }

    @Test
    public void testSecondPlayerWinsByTheRules() {
        rockScissorsPaper.setFirstPlayer(player1);
        rockScissorsPaper.setSecondPlayer(player2);
        rockScissorsPaper.makeTurn(player1, "SCISSORS");
        rockScissorsPaper.makeTurn(player2, "ROCK");

        assertTrue(secondPlayerWins());
    }

    @Test
    public void testSecondPlayerLosesByTheRules() {
        rockScissorsPaper.setFirstPlayer(player1);
        rockScissorsPaper.setSecondPlayer(player2);
        rockScissorsPaper.makeTurn(player1, "SCISSORS");
        rockScissorsPaper.makeTurn(player2, "PAPER");

        assertFalse(secondPlayerWins());
    }

    @Test
    public void testSecondPlayerDrawsByTheRules() {
        rockScissorsPaper.setFirstPlayer(player1);
        rockScissorsPaper.setSecondPlayer(player2);
        rockScissorsPaper.makeTurn(player1, "ROCK");
        rockScissorsPaper.makeTurn(player2, "ROCK");

        assertTrue(happensDraw());
    }

    @Test(expected = NullPointerException.class)
    public void testSecondPlayerFailsIfTurnIsNullAndTrowsNullPointerEx() {
        rockScissorsPaper.setFirstPlayer(player1);
        rockScissorsPaper.setSecondPlayer(player2);
        rockScissorsPaper.makeTurn(player1, null);
        rockScissorsPaper.makeTurn(player2, "SPOCK");

        assertFalse(secondPlayerWins());
    }
}
