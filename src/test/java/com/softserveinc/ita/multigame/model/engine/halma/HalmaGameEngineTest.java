package com.softserveinc.ita.multigame.model.engine.halma;

import com.softserveinc.ita.multigame.model.Player;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

/**
 * Created by vvserdiuk on 03.10.2016.
 */
public class HalmaGameEngineTest {

    HalmaGameEngine halmaGameEngine;
    Player firstPlayer;
    Player secondPlayer;

    @Before
    public void setUp() throws Exception {
        halmaGameEngine = new HalmaGameEngine();
        firstPlayer = new Player("Gin", "123");
        firstPlayer.setId(1L);
        secondPlayer = new Player("Tonic", "123");
        secondPlayer.setId(2L);
    }

    @Test
    public void isBoardInitializedWell(){
        Cell cell = halmaGameEngine.cells.get(0);

        assertThat(halmaGameEngine.cells.size(), is(256));
        assertThat(halmaGameEngine.cells.get(0).address, is("A1"));
        assertThat(halmaGameEngine.cells.get(15).address, is("P1"));
        assertThat(halmaGameEngine.cells.get(240).address, is("A16"));
        assertThat(halmaGameEngine.cells.get(255).address, is("P16"));
    }
}
