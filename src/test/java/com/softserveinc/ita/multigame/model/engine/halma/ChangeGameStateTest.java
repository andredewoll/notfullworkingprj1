package com.softserveinc.ita.multigame.model.engine.halma;

import com.softserveinc.ita.multigame.model.engine.GameResultCode;
import com.softserveinc.ita.multigame.model.engine.GameState;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by vvserdiuk on 06.10.2016.
 */
public class ChangeGameStateTest extends HalmaGameEngineTest {

    @Test
    public void turnToNearestDownCellByByFirstPlayer(){
        halmaGameEngine.setFirstPlayer(firstPlayer);
        halmaGameEngine.setSecondPlayer(secondPlayer);

        GameState actualState = halmaGameEngine.changeGameState(firstPlayer, "A5,A6");
        assertThat(halmaGameEngine.getCellByAddress("A5").state, is(CellState.EMPTY));
        assertThat(halmaGameEngine.getCellByAddress("A6").state, is(CellState.BUSY_BY_FIRST_PLAYER));
        assertThat(actualState, is(GameState.WAIT_FOR_SECOND_PLAYER_TURN));
    }

    @Test
    public void turnToNearestUpCellByByFirstPlayer(){
        halmaGameEngine.setFirstPlayer(firstPlayer);         halmaGameEngine.setSecondPlayer(secondPlayer); 
        GameState actualState = halmaGameEngine.changeGameState(firstPlayer, "B8,B7");

        assertThat(halmaGameEngine.getCellByAddress("B8").state, is(CellState.EMPTY));
        assertThat(halmaGameEngine.getCellByAddress("B7").state, is(CellState.BUSY_BY_FIRST_PLAYER));
        assertThat(actualState, is(GameState.WAIT_FOR_SECOND_PLAYER_TURN));
    }

    @Test
    public void turnToNearestLeftCellByByFirstPlayer(){
        halmaGameEngine.setFirstPlayer(firstPlayer);         halmaGameEngine.setSecondPlayer(secondPlayer); 
        halmaGameEngine.getCellByAddress("C6").setState(CellState.BUSY_BY_FIRST_PLAYER);
        GameState actualState = halmaGameEngine.changeGameState(firstPlayer, "C6,B6");

        assertThat(halmaGameEngine.getCellByAddress("C6").state, is(CellState.EMPTY));
        assertThat(halmaGameEngine.getCellByAddress("B6").state, is(CellState.BUSY_BY_FIRST_PLAYER));
        assertThat(actualState, is(GameState.WAIT_FOR_SECOND_PLAYER_TURN));
    }

    @Test
    public void turnToNearestRightCellByByFirstPlayer(){
        halmaGameEngine.setFirstPlayer(firstPlayer);         halmaGameEngine.setSecondPlayer(secondPlayer); 
        GameState actualState = halmaGameEngine.changeGameState(firstPlayer, "E1,F1");

        assertThat(halmaGameEngine.getCellByAddress("E1").state, is(CellState.EMPTY));
        assertThat(halmaGameEngine.getCellByAddress("F1").state, is(CellState.BUSY_BY_FIRST_PLAYER));
        assertThat(actualState, is(GameState.WAIT_FOR_SECOND_PLAYER_TURN));
    }


    @Test
    public void turnToNearestDownCellBySecondPlayer(){
        halmaGameEngine.setFirstPlayer(firstPlayer);
        halmaGameEngine.setSecondPlayer(secondPlayer);
        halmaGameEngine.makeTurn(firstPlayer, "A5,A6");
        GameState actualState = halmaGameEngine.changeGameState(secondPlayer, "P10,P11");

        assertThat(halmaGameEngine.getCellByAddress("P10").state, is(CellState.EMPTY));
        assertThat(halmaGameEngine.getCellByAddress("P11").state, is(CellState.BUSY_BY_SECOND_PLAYER));
        assertThat(actualState, is(GameState.WAIT_FOR_FIRST_PLAYER_TURN));
    }
    @Test
    public void turnToNearestUpCellBySecondPlayer(){
         halmaGameEngine.setFirstPlayer(firstPlayer);         halmaGameEngine.setSecondPlayer(secondPlayer);         halmaGameEngine.makeTurn(firstPlayer, "A5,A6");
        GameState actualState = halmaGameEngine.changeGameState(secondPlayer, "M14,M13");

        assertThat(halmaGameEngine.getCellByAddress("M14").state, is(CellState.EMPTY));
        assertThat(halmaGameEngine.getCellByAddress("M13").state, is(CellState.BUSY_BY_SECOND_PLAYER));
        assertThat(actualState, is(GameState.WAIT_FOR_FIRST_PLAYER_TURN));
    }

    @Test
    public void turnToNearestLeftCellBySecondPlayer(){
         halmaGameEngine.setFirstPlayer(firstPlayer);         halmaGameEngine.setSecondPlayer(secondPlayer);         halmaGameEngine.makeTurn(firstPlayer, "A5,A6");
        GameState actualState = halmaGameEngine.changeGameState(secondPlayer, "G10,F10");

        assertThat(halmaGameEngine.getCellByAddress("G10").state, is(CellState.EMPTY));
        assertThat(halmaGameEngine.getCellByAddress("F10").state, is(CellState.BUSY_BY_SECOND_PLAYER));
        assertThat(actualState, is(GameState.WAIT_FOR_FIRST_PLAYER_TURN));
    }

    @Test
    public void turnToNearestRightCellBySecondPlayer(){
         halmaGameEngine.setFirstPlayer(firstPlayer);         halmaGameEngine.setSecondPlayer(secondPlayer);         halmaGameEngine.makeTurn(firstPlayer, "A5,A6");
        GameState actualState = halmaGameEngine.changeGameState(secondPlayer, "M11,N11");

        assertThat(halmaGameEngine.getCellByAddress("M11").state, is(CellState.EMPTY));
        assertThat(halmaGameEngine.getCellByAddress("N11").state, is(CellState.BUSY_BY_SECOND_PLAYER));
        assertThat(actualState, is(GameState.WAIT_FOR_FIRST_PLAYER_TURN));
    }


    @Test
    public void jumpDownByByFirstPlayer() {
        halmaGameEngine.setFirstPlayer(firstPlayer);         halmaGameEngine.setSecondPlayer(secondPlayer); 
        GameState actualState = halmaGameEngine.changeGameState(secondPlayer, "A4,A6");

        assertThat(halmaGameEngine.getCellByAddress("A4").state, is(CellState.EMPTY));
        assertThat(halmaGameEngine.getCellByAddress("A6").state, is(CellState.BUSY_BY_FIRST_PLAYER));
        assertThat(actualState, is(GameState.WAIT_FOR_FIRST_PLAYER_TURN));
    }

    @Test
    public void jumpUpByByFirstPlayer() {
        halmaGameEngine.setFirstPlayer(firstPlayer);         halmaGameEngine.setSecondPlayer(secondPlayer); 
        GameState actualState = halmaGameEngine.changeGameState(secondPlayer, "L8,L6");

        assertThat(halmaGameEngine.getCellByAddress("L8").state, is(CellState.EMPTY));
        assertThat(halmaGameEngine.getCellByAddress("L6").state, is(CellState.BUSY_BY_FIRST_PLAYER));
        assertThat(actualState, is(GameState.WAIT_FOR_FIRST_PLAYER_TURN));
    }

    @Test
    public void jumpLeftByByFirstPlayer() {
        halmaGameEngine.setFirstPlayer(firstPlayer);         halmaGameEngine.setSecondPlayer(secondPlayer); 
        GameState actualState = halmaGameEngine.changeGameState(secondPlayer, "L8,J8");

        assertThat(halmaGameEngine.getCellByAddress("L8").state, is(CellState.EMPTY));
        assertThat(halmaGameEngine.getCellByAddress("J8").state, is(CellState.BUSY_BY_FIRST_PLAYER));
        assertThat(actualState, is(GameState.WAIT_FOR_FIRST_PLAYER_TURN));
    }

    @Test
    public void jumpRightByByFirstPlayer() {
        halmaGameEngine.setFirstPlayer(firstPlayer);         halmaGameEngine.setSecondPlayer(secondPlayer); 
        GameState actualState = halmaGameEngine.changeGameState(secondPlayer, "D3,F3");

        assertThat(halmaGameEngine.getCellByAddress("D3").state, is(CellState.EMPTY));
        assertThat(halmaGameEngine.getCellByAddress("F3").state, is(CellState.BUSY_BY_FIRST_PLAYER));
        assertThat(actualState, is(GameState.WAIT_FOR_FIRST_PLAYER_TURN));
    }

    @Test
    public void jumpDownBySecondPlayer() {
         halmaGameEngine.setFirstPlayer(firstPlayer);         halmaGameEngine.setSecondPlayer(secondPlayer);         halmaGameEngine.makeTurn(firstPlayer, "A5,A6");
        GameState actualState = halmaGameEngine.changeGameState(secondPlayer, "A4,A6");

        assertThat(halmaGameEngine.getCellByAddress("A4").state, is(CellState.EMPTY));
        assertThat(halmaGameEngine.getCellByAddress("A6").state, is(CellState.BUSY_BY_SECOND_PLAYER));
        assertThat(actualState, is(GameState.WAIT_FOR_SECOND_PLAYER_TURN));
    }

    @Test
    public void jumpUpBySecondPlayer() {
         halmaGameEngine.setFirstPlayer(firstPlayer);         halmaGameEngine.setSecondPlayer(secondPlayer);         halmaGameEngine.makeTurn(firstPlayer, "A5,A6");
        GameState actualState = halmaGameEngine.changeGameState(secondPlayer, "L8,L6");

        assertThat(halmaGameEngine.getCellByAddress("L8").state, is(CellState.EMPTY));
        assertThat(halmaGameEngine.getCellByAddress("L6").state, is(CellState.BUSY_BY_SECOND_PLAYER));
        assertThat(actualState, is(GameState.WAIT_FOR_SECOND_PLAYER_TURN));
    }

    @Test
    public void jumpLeftSecondPlayer() {
         halmaGameEngine.setFirstPlayer(firstPlayer);         halmaGameEngine.setSecondPlayer(secondPlayer);         halmaGameEngine.makeTurn(firstPlayer, "A5,A6");
        GameState actualState = halmaGameEngine.changeGameState(secondPlayer, "L8,J8");

        assertThat(halmaGameEngine.getCellByAddress("L8").state, is(CellState.EMPTY));
        assertThat(halmaGameEngine.getCellByAddress("J8").state, is(CellState.BUSY_BY_SECOND_PLAYER));
        assertThat(actualState, is(GameState.WAIT_FOR_SECOND_PLAYER_TURN));
    }

    @Test
    public void jumpRightSecondPlayer() {
         halmaGameEngine.setFirstPlayer(firstPlayer);         halmaGameEngine.setSecondPlayer(secondPlayer);         halmaGameEngine.makeTurn(firstPlayer, "A5,A6");
        GameState actualState = halmaGameEngine.changeGameState(secondPlayer, "D3,F3");

        assertThat(halmaGameEngine.getCellByAddress("D3").state, is(CellState.EMPTY));
        assertThat(halmaGameEngine.getCellByAddress("F3").state, is(CellState.BUSY_BY_SECOND_PLAYER));
        assertThat(actualState, is(GameState.WAIT_FOR_SECOND_PLAYER_TURN));
    }


    @Test
    public void jumpAfterJumpByFirstPlayer(){
        halmaGameEngine.setFirstPlayer(firstPlayer);         halmaGameEngine.setSecondPlayer(secondPlayer); 
        halmaGameEngine.cellToContinueTurn = new Cell("A5");
        GameState actualState = halmaGameEngine.changeGameState(secondPlayer, "A5,A7");

        assertThat(halmaGameEngine.getCellByAddress("A5").state, is(CellState.EMPTY));
        assertThat(halmaGameEngine.getCellByAddress("A7").state, is(CellState.BUSY_BY_FIRST_PLAYER));
        assertThat(actualState, is(GameState.WAIT_FOR_FIRST_PLAYER_TURN));
    }

    @Test
    public void jumpAfterJumpSecondPlayer(){
         halmaGameEngine.setFirstPlayer(firstPlayer);         halmaGameEngine.setSecondPlayer(secondPlayer);         halmaGameEngine.makeTurn(firstPlayer, "A5,A6");
        halmaGameEngine.cellToContinueTurn = new Cell("A5");
        GameState actualState = halmaGameEngine.changeGameState(secondPlayer, "A5,A7");

        assertThat(halmaGameEngine.getCellByAddress("A5").state, is(CellState.EMPTY));
        assertThat(halmaGameEngine.getCellByAddress("A7").state, is(CellState.BUSY_BY_SECOND_PLAYER));
        assertThat(actualState, is(GameState.WAIT_FOR_SECOND_PLAYER_TURN));
        assertThat(halmaGameEngine.getResultCode(), is(GameResultCode.OK));
    }


    @Test
    public void turnToNearestAfterJumpByFirstPlayer(){
        halmaGameEngine.setFirstPlayer(firstPlayer);
        halmaGameEngine.setSecondPlayer(secondPlayer);
        halmaGameEngine.makeTurn(firstPlayer, "B4,B6");
        GameState actualState = halmaGameEngine.changeGameState(firstPlayer, "B6,C6");

        assertThat(halmaGameEngine.getCellByAddress("B6").state, is(CellState.BUSY_BY_FIRST_PLAYER));
        assertThat(halmaGameEngine.getCellByAddress("C6").state, is(CellState.EMPTY));
        assertThat(actualState, is(GameState.WAIT_FOR_FIRST_PLAYER_TURN));
    }

    @Test
    public void turnToNearestAfterJumpSecondPlayer(){
         halmaGameEngine.setFirstPlayer(firstPlayer);         halmaGameEngine.setSecondPlayer(secondPlayer);         halmaGameEngine.makeTurn(firstPlayer, "A5,A6");
        halmaGameEngine.cellToContinueTurn = new Cell("P12");
        GameState actualState = halmaGameEngine.changeGameState(secondPlayer, "P12,P11");

        assertThat(halmaGameEngine.getCellByAddress("P12").state, is(CellState.BUSY_BY_SECOND_PLAYER));
        assertThat(halmaGameEngine.getCellByAddress("P11").state, is(CellState.EMPTY));
        assertThat(actualState, is(GameState.WAIT_FOR_SECOND_PLAYER_TURN));
    }

    @Test
    public void stayOnSamePlaceAfterJumpByFirstPlayer(){
        halmaGameEngine.setFirstPlayer(firstPlayer);         halmaGameEngine.setSecondPlayer(secondPlayer); 
        halmaGameEngine.cellToContinueTurn = new Cell("D3");
        GameState actualState = halmaGameEngine.changeGameState(secondPlayer, "D3,D3");

        assertThat(halmaGameEngine.getCellByAddress("D3").state, is(CellState.BUSY_BY_FIRST_PLAYER));
        assertThat(actualState, is(GameState.WAIT_FOR_SECOND_PLAYER_TURN));
        assertThat(halmaGameEngine.getResultCode(), is(GameResultCode.OK));
    }

    @Test
    public void stayOnSamePlaceAfterJumpSecondPlayer(){
         halmaGameEngine.setFirstPlayer(firstPlayer);         halmaGameEngine.setSecondPlayer(secondPlayer);         halmaGameEngine.makeTurn(firstPlayer, "A5,A6");
        halmaGameEngine.cellToContinueTurn = new Cell("P12");
        GameState actualState = halmaGameEngine.changeGameState(secondPlayer, "P12,P12");

        assertThat(halmaGameEngine.getCellByAddress("P12").state, is(CellState.BUSY_BY_SECOND_PLAYER));
        assertThat(actualState, is(GameState.WAIT_FOR_FIRST_PLAYER_TURN));
        assertThat(halmaGameEngine.getResultCode(), is(GameResultCode.OK));
    }

    @Test
    public void tryToJumpSecondTimeNotFromRightCellByFirstPlayer(){
        halmaGameEngine.setFirstPlayer(firstPlayer);
        halmaGameEngine.setSecondPlayer(secondPlayer);
        halmaGameEngine.cellToContinueTurn = new Cell("B5");
        GameState actualState = halmaGameEngine.changeGameState(secondPlayer, "A5,C5");

        assertThat(halmaGameEngine.getCellByAddress("B5").state, is(CellState.BUSY_BY_FIRST_PLAYER));
        assertThat(halmaGameEngine.getCellByAddress("C5").state, is(CellState.EMPTY));
        assertThat(halmaGameEngine.getCellByAddress("A5").state, is(CellState.BUSY_BY_FIRST_PLAYER));
        assertThat(actualState, is(GameState.WAIT_FOR_FIRST_PLAYER_TURN));
    }

    @Test
    public void tryToJumpSecondTimeNotFromRightCellSecondPlayer(){
        halmaGameEngine.setFirstPlayer(firstPlayer);
        halmaGameEngine.setSecondPlayer(secondPlayer);
        halmaGameEngine.makeTurn(firstPlayer, "A5,A6");
        halmaGameEngine.cellToContinueTurn = new Cell("K10");
        halmaGameEngine.getCellByAddress("K10").setState(CellState.BUSY_BY_SECOND_PLAYER);
        GameState actualState = halmaGameEngine.changeGameState(secondPlayer, "P12,P11");

        assertThat(halmaGameEngine.getCellByAddress("K10").state, is(CellState.BUSY_BY_SECOND_PLAYER));
        assertThat(halmaGameEngine.getCellByAddress("P12").state, is(CellState.BUSY_BY_SECOND_PLAYER));
        assertThat(halmaGameEngine.getCellByAddress("P11").state, is(CellState.EMPTY));
        assertThat(actualState, is(GameState.WAIT_FOR_SECOND_PLAYER_TURN));
    }

    @Test
    public void turnMakesGameFinishedByByFirstPlayer(){
        halmaGameEngine.setFirstPlayer(firstPlayer);         halmaGameEngine.setSecondPlayer(secondPlayer); 
        halmaGameEngine.getCellByAddress("P12").setState(CellState.BUSY_BY_FIRST_PLAYER);
        halmaGameEngine.getCellByAddress("P13").setState(CellState.BUSY_BY_FIRST_PLAYER);
        halmaGameEngine.getCellByAddress("P14").setState(CellState.BUSY_BY_FIRST_PLAYER);
        halmaGameEngine.getCellByAddress("P15").setState(CellState.BUSY_BY_FIRST_PLAYER);
        halmaGameEngine.getCellByAddress("P16").setState(CellState.BUSY_BY_FIRST_PLAYER);
        halmaGameEngine.getCellByAddress("O12").setState(CellState.EMPTY);
        halmaGameEngine.getCellByAddress("O13").setState(CellState.BUSY_BY_FIRST_PLAYER);
        halmaGameEngine.getCellByAddress("O14").setState(CellState.BUSY_BY_FIRST_PLAYER);
        halmaGameEngine.getCellByAddress("O15").setState(CellState.BUSY_BY_FIRST_PLAYER);
        halmaGameEngine.getCellByAddress("O16").setState(CellState.BUSY_BY_FIRST_PLAYER);
        halmaGameEngine.getCellByAddress("N13").setState(CellState.BUSY_BY_FIRST_PLAYER);
        halmaGameEngine.getCellByAddress("N14").setState(CellState.BUSY_BY_FIRST_PLAYER);
        halmaGameEngine.getCellByAddress("N15").setState(CellState.BUSY_BY_FIRST_PLAYER);
        halmaGameEngine.getCellByAddress("N16").setState(CellState.BUSY_BY_FIRST_PLAYER);
        halmaGameEngine.getCellByAddress("M14").setState(CellState.BUSY_BY_FIRST_PLAYER);
        halmaGameEngine.getCellByAddress("M15").setState(CellState.BUSY_BY_FIRST_PLAYER);
        halmaGameEngine.getCellByAddress("M16").setState(CellState.BUSY_BY_FIRST_PLAYER);
        halmaGameEngine.getCellByAddress("L15").setState(CellState.BUSY_BY_FIRST_PLAYER);
        halmaGameEngine.getCellByAddress("L16").setState(CellState.BUSY_BY_FIRST_PLAYER);

        GameState actualState = halmaGameEngine.changeGameState(secondPlayer, "O11,O12");

        assertThat(halmaGameEngine.getCellByAddress("O11").state, is(CellState.EMPTY));
        assertThat(halmaGameEngine.getCellByAddress("O12").state, is(CellState.BUSY_BY_FIRST_PLAYER));
        assertThat(actualState, is(GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER));
    }

    @Test
    public void turnMakesGameFinishedByBySecondPlayer(){
         halmaGameEngine.setFirstPlayer(firstPlayer);         halmaGameEngine.setSecondPlayer(secondPlayer);         halmaGameEngine.makeTurn(firstPlayer, "A5,A6");
        halmaGameEngine.getCellByAddress("A1").setState(CellState.BUSY_BY_SECOND_PLAYER);
        halmaGameEngine.getCellByAddress("A2").setState(CellState.BUSY_BY_SECOND_PLAYER);
        halmaGameEngine.getCellByAddress("A3").setState(CellState.BUSY_BY_SECOND_PLAYER);
        halmaGameEngine.getCellByAddress("A4").setState(CellState.BUSY_BY_SECOND_PLAYER);
        halmaGameEngine.getCellByAddress("A5").setState(CellState.BUSY_BY_SECOND_PLAYER);
        halmaGameEngine.getCellByAddress("B1").setState(CellState.BUSY_BY_SECOND_PLAYER);
        halmaGameEngine.getCellByAddress("B2").setState(CellState.BUSY_BY_SECOND_PLAYER);
        halmaGameEngine.getCellByAddress("B3").setState(CellState.BUSY_BY_SECOND_PLAYER);
        halmaGameEngine.getCellByAddress("B4").setState(CellState.BUSY_BY_SECOND_PLAYER);
        halmaGameEngine.getCellByAddress("B5").setState(CellState.EMPTY);
        halmaGameEngine.getCellByAddress("C1").setState(CellState.BUSY_BY_SECOND_PLAYER);
        halmaGameEngine.getCellByAddress("C2").setState(CellState.BUSY_BY_SECOND_PLAYER);
        halmaGameEngine.getCellByAddress("C3").setState(CellState.BUSY_BY_SECOND_PLAYER);
        halmaGameEngine.getCellByAddress("C4").setState(CellState.BUSY_BY_SECOND_PLAYER);
        halmaGameEngine.getCellByAddress("D1").setState(CellState.BUSY_BY_SECOND_PLAYER);
        halmaGameEngine.getCellByAddress("D2").setState(CellState.BUSY_BY_SECOND_PLAYER);
        halmaGameEngine.getCellByAddress("D3").setState(CellState.BUSY_BY_SECOND_PLAYER);
        halmaGameEngine.getCellByAddress("E1").setState(CellState.BUSY_BY_SECOND_PLAYER);
        halmaGameEngine.getCellByAddress("E2").setState(CellState.BUSY_BY_SECOND_PLAYER);

        GameState actualState = halmaGameEngine.changeGameState(secondPlayer, "C6,B5");

        assertThat(halmaGameEngine.getCellByAddress("C5").state, is(CellState.EMPTY));
        assertThat(halmaGameEngine.getCellByAddress("B5").state, is(CellState.BUSY_BY_SECOND_PLAYER));
        assertThat(actualState, is(GameState.FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER));
    }
}
