package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameListManager;
import com.softserveinc.ita.multigame.model.GameType;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.repositories.PlayerRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by vvserdiuk on 27.11.2016.
 */
public class PlayerServiceTest {

    @InjectMocks
    PlayerService playerService = new PlayerServiceImpl();
    @Mock
    PlayerRepository playerRepository;
    @Mock
    GameHistoryService gameHistoryService;

    Player firstPlayer = new Player(1L, "Gin", "123");
    Player secondPlayer = new Player(2L, "Tonic", "123");

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGet() throws Exception {
        when(playerRepository.findOne(1L)).thenReturn(firstPlayer);

        Player actualPlayer = playerService.get(1L);

        verify(playerRepository).findOne(1L);
        assertThat(actualPlayer, is(firstPlayer));
    }

    @Test
    public void testGetByLogin() throws Exception {
        when(playerRepository.findByLogin("Gin")).thenReturn(firstPlayer);

        Player actualPlayer = playerService.getByLogin("Gin");

        verify(playerRepository).findByLogin("Gin");
        assertThat(actualPlayer, is(firstPlayer));
    }

    @Test
    public void testGetAll() throws Exception {
        when(playerRepository.findAll()).thenReturn(Arrays.asList(firstPlayer, secondPlayer));

        List<Player> actualPlayers = playerService.getAll();

        verify(playerRepository).findAll();
        assertThat(actualPlayers, is(Arrays.asList(firstPlayer, secondPlayer)));
    }

    @Test
    public void testDeleteByPlayer() throws Exception {
        playerService.delete(firstPlayer);

        verify(gameHistoryService).removeGameHistoriesForPlayer(firstPlayer);
        verify(playerRepository).delete(firstPlayer);
    }

    @Test
    public void testDeleteById() throws Exception {
        when(playerRepository.findOne(1L)).thenReturn(firstPlayer);

        playerService.delete(1L);

        verify(gameHistoryService).removeGameHistoriesForPlayer(firstPlayer);
        verify(playerRepository).delete(1L);
    }

    @Test
    public void testSaveOrUpdate() throws Exception {
        when(playerRepository.save(firstPlayer)).thenReturn(firstPlayer);

        Player actualPlayer = playerService.saveOrUpdate(firstPlayer);

        verify(playerRepository).save(firstPlayer);
        assertThat(actualPlayer, is(firstPlayer));
    }
}