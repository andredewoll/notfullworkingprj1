package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameListManager;
import com.softserveinc.ita.multigame.model.GameType;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameDTO;
import com.softserveinc.ita.multigame.model.engine.GameEngine;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GameListServiceTest {

    @InjectMocks
    GameListService gameListService = new GameListServiceImpl();
    @Mock
    GameListManager gameListManager;
    @Mock
    GameHistoryService gameHistoryService;
    GameType gameType = GameType.HALMA;
    Player firstPlayer = new Player(1L, "Gin", "123");
    Player secondPlayer = new Player(2L, "Tonic", "123");
    @Mock
    Game gameMock;
    Game game0 = new Game(firstPlayer, gameType);
    Game game1 = new Game(firstPlayer, gameType);
    Game game2 = new Game(firstPlayer, gameType);

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateGame() {
        gameListService.createGame(firstPlayer, gameType);

        verify(gameListManager).createGame(firstPlayer, gameType);
    }

    @Test
    public void testCreateAndReturnGame() {
        when(gameListManager.createAndReturnGame(firstPlayer, gameType)).thenReturn(game0);

        Game actual = gameListService.createAndReturnGame(firstPlayer, gameType);

        verify(gameListManager).createAndReturnGame(firstPlayer, gameType);
        assertThat(actual, is(game0));
    }

    @Test
    public void testDeleteGame() {
        gameListService.deleteGame(1L);

        verify(gameListManager).deleteGame(1L);
    }

    @Test
    public void testGetGame() {
        when(gameListManager.getGame(1L)).thenReturn(game0);

        Game actual = gameListService.getGame(1L);

        verify(gameListManager).getGame(1L);
        assertThat(actual, is(game0));
    }

    @Test
    public void testGetPlayingGamesDTOs() {
        when(gameListManager.getPlayingGamesIds(firstPlayer)).thenReturn(Arrays.asList(0L, 1L, 2L));
        when(gameListManager.getGame(0L)).thenReturn(game0);
        when(gameListManager.getGame(1L)).thenReturn(game1);
        when(gameListManager.getGame(2L)).thenReturn(game2);
        List<GameDTO> expected = Arrays.asList(new GameDTO(game0), new GameDTO(game1), new GameDTO(game2));

        List<GameDTO> actual = gameListService.getPlayingGamesDTOs(firstPlayer);

        assertThat(actual, is(expected));
    }

    @Test
    public void testGetCreatedGamesDTOs() {
        when(gameListManager.getCreatedGamesIds(firstPlayer)).thenReturn(Arrays.asList(0L, 1L, 2L));
        when(gameListManager.getGame(0L)).thenReturn(game0);
        when(gameListManager.getGame(1L)).thenReturn(game1);
        when(gameListManager.getGame(2L)).thenReturn(game2);
        List<GameDTO> expected = Arrays.asList(new GameDTO(game0), new GameDTO(game1), new GameDTO(game2));

        List<GameDTO> actual = gameListService.getCreatedGamesDTOs(firstPlayer);

        assertThat(actual, is(expected));
    }
    @Test
    public void testGetWaitingGamesDTOs() {
        when(gameListManager.getWaitingGamesIds(firstPlayer)).thenReturn(Arrays.asList(0L, 1L, 2L));
        when(gameListManager.getGame(0L)).thenReturn(game0);
        when(gameListManager.getGame(1L)).thenReturn(game1);
        when(gameListManager.getGame(2L)).thenReturn(game2);
        List<GameDTO> expected = Arrays.asList(new GameDTO(game0), new GameDTO(game1), new GameDTO(game2));

        List<GameDTO> actual = gameListService.getWaitingGamesDTOs(firstPlayer);

        assertThat(actual, is(expected));
    }

    @Test
    public void testMakeTurnReturnFalseIfGameReturnFalse() {
        when(gameListManager.getGame(0L)).thenReturn(gameMock);
        when(gameMock.makeTurn(firstPlayer, "A5,A6")).thenReturn(false);

        boolean actual = gameListService.makeTurn(0L, firstPlayer, "A5,A6");

        assertThat(actual, is(false));
    }

    @Test
    public void testMakeTurnReturnTrueIfGameReturnTrueAndGameNotFinished() {
        when(gameListManager.getGame(0L)).thenReturn(gameMock);
        when(gameMock.makeTurn(firstPlayer, "A5,A6")).thenReturn(true);
        GameEngine gameEngine = mock(GameEngine.class);
        when(gameMock.getGameEngine()).thenReturn(gameEngine);
        when(gameEngine.isFinished()).thenReturn(false);

        boolean actual = gameListService.makeTurn(0L, firstPlayer, "A5,A6");

        assertThat(actual, is(true));
    }

    @Test
    public void testMakeTurnReturnTrueIfGameReturnTrueAndFinishedGameSavedToDB() {
        when(gameListManager.getGame(0L)).thenReturn(gameMock);
        when(gameMock.makeTurn(firstPlayer, "A5,A6")).thenReturn(true);
        GameEngine gameEngine = mock(GameEngine.class);
        when(gameMock.getGameEngine()).thenReturn(gameEngine);
        when(gameEngine.isFinished()).thenReturn(true);

        boolean actual = gameListService.makeTurn(0L, firstPlayer, "A5,A6");

        verify(gameHistoryService).saveGameHistoryForGame(gameMock);
        verify(gameListManager).deleteGame(0L);
        assertThat(actual, is(true));
    }

}